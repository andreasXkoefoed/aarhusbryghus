/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package storage;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import application.model.Kunde;
import application.model.Ordre;
import application.model.Ordrelinje;
import application.model.Produkt;
import application.model.ProduktKategori;
import application.model.Salgssituation;

// Singleton implementering
public class Storage implements Serializable {
	private static Storage storage;

	public static Storage getInstance() {
		if (storage == null) {
			storage = new Storage();
		}
		return storage;
	}

	private Storage() {
	}

	// -------------------------------------------------------------------------

	private List<ProduktKategori> kategorier = new ArrayList<>();
	private List<Produkt> produkter = new ArrayList<>();
	private List<Salgssituation> salgssituation = new ArrayList<>();
	private List<Ordre> ordre = new ArrayList<>();
	private List<Kunde> kunder = new ArrayList<>();
	private List<Ordrelinje> afleverede = new ArrayList<>();

	// Salgssituation get store delete

	public ArrayList<Salgssituation> getSalgssituation() {
		return new ArrayList<>(this.salgssituation);
	}

	public void storeSalgssituation(Salgssituation salgssituation) {
		this.salgssituation.add(salgssituation);
	}

	public void deleteSalgssituation(Salgssituation salgssituation) {
		this.salgssituation.remove(salgssituation);
	}

	// Produkt Kategori, get, store, delete
	public ArrayList<ProduktKategori> getKategorier() {
		return new ArrayList<ProduktKategori>(this.kategorier);
	}

	public void storeProduktKategori(ProduktKategori kategori) {
		this.kategorier.add(kategori);
	}

	public void deleteProduktKategori(ProduktKategori kategori) {
		this.kategorier.remove(kategori);
	}

	// Produkt, get, store, delete
	public ArrayList<Produkt> getProdukter() {
		return new ArrayList<>(this.produkter);
	}

	public void storeProdukt(Produkt produkt) {
		this.produkter.add(produkt);
	}

	public void deleteProdukt(Produkt produkt) {
		this.produkter.remove(produkt);
	}

	// Ordre start ---------------------------
	public void storeOrdre(Ordre ordre) {
		this.ordre.add(ordre);
	}

	public void deleteOrdre(Ordre ordre) {
		this.ordre.remove(ordre);
	}

	public ArrayList<Ordre> getOrdre() {
		return new ArrayList<>(this.ordre);
	}

	// Udlejning
	public void storeAfleveredeUdlejedeProdukter(Ordrelinje ordrelinje) {
		this.afleverede.add(ordrelinje);
	}

	public ArrayList<Ordrelinje> getAfleverede() {
		return new ArrayList<>(this.afleverede);
	}
	// Ordre slut -------------------------

	// Kunde start
	public void storeKunde(Kunde kunde) {
		this.kunder.add(kunde);
	}

	public void deleteKunde(Kunde kunde) {
		this.kunder.remove(kunde);
	}

	public ArrayList<Kunde> getKunder() {
		return new ArrayList<>(this.kunder);
	}
	// -------------------------------------------------------------------------

	/**
	 * Loads the storage (including all objects in storage).
	 */
	public void load() {
		try (FileInputStream fileIn = new FileInputStream("storage.ser");
				ObjectInputStream in = new ObjectInputStream(fileIn)) {
			storage = (Storage) in.readObject();
			System.out.println("Storage loaded from file storage.ser.");
		} catch (ClassNotFoundException ex) {
			System.out.println("Error loading storage object.");
			throw new RuntimeException(ex.getMessage());
		} catch (IOException ex) {
			System.out.println("Error loading storage object.");
			throw new RuntimeException(ex.getMessage());
		}
	}

	/**
	 * Saves the storage (including all objects in storage).
	 */
	public void save() {
		try (FileOutputStream fileOut = new FileOutputStream("storage.ser");
				ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
			out.writeObject(storage);
			System.out.println("Storage saved in file storage.ser.");
		} catch (IOException ex) {
			System.out.println("Error saving storage object.");
			throw new RuntimeException(ex.getMessage());
		}
	}

	/** Clears the storage. */
	public void clear() {
		storage = new Storage();
	}
}
