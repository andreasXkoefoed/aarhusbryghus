/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package setup;

import java.time.LocalDate;

import application.controller.Controller;
import application.model.Enhed;
import application.model.Kunde;
import application.model.Produkt;
import application.model.ProduktKategori;
import application.model.Salgssituation;
import application.model.Udlejning;

public class StorageInitializer {

	public static void main(String[] args) {
		initStorage();
	}

	/** Initialises the storage with some objects. */
	private static void initStorage() {

		ProduktKategori flaske = Controller.createProduktKategori("flaske", Enhed.CL, false, 2);
		Produkt klosterbryg = Controller.createProdukt(flaske, "Klosterbryg", 36, "beskrivelse");
		Produkt SGB = Controller.createProdukt(flaske, "Sweet Georgia Brown", 36, "Beskrivelse");
		Produkt extraPilsner = Controller.createProdukt(flaske, "Extra Pilsner", 36, "beskrivelse");
		Produkt celebration = Controller.createProdukt(flaske, "Celebration", 36, "beskrivelse");
		Produkt blondie = Controller.createProdukt(flaske, "Blondie", 36, "beskrivelse");
		Produkt foraarsbryg = Controller.createProdukt(flaske, "Forårsbryg", 36, "beskrivelse");
		Produkt indiaPaleAle = Controller.createProdukt(flaske, "India Pale Ale", 36, "beskrivelse");
		Produkt julebryg = Controller.createProdukt(flaske, "Julebryg", 36, "beskrivelse");
		Produkt juletonden = Controller.createProdukt(flaske, "Juletønden", 36, "beskrivelse");
		Produkt oldStrongAle = Controller.createProdukt(flaske, "Old Strong Ale", 36, "beskrivelse");
		Produkt fregattenJylland = Controller.createProdukt(flaske, "Fregatten Jylland", 36, "beskrivelse");
		Produkt imperialStout = Controller.createProdukt(flaske, "Imperial Stout", 36, "beskrivelse");
		Produkt tribute = Controller.createProdukt(flaske, "Tribute", 36, "beskrivelse");
		Produkt blackMonster = Controller.createProdukt(flaske, "Black Monster", 50, "beskrivelse");

		ProduktKategori fadol = Controller.createProduktKategori("Fadøl", Enhed.GLAS, false, 2);
		Produkt fadol_klosterbryg = Controller.createProdukt(fadol, "Klosterbryg", 30, "beskrivelse");
		Produkt fadol_jazzClassic = Controller.createProdukt(fadol, "Jazz Classic", 30, "beskrivelse");
		Produkt fadol_extraPilsner = Controller.createProdukt(fadol, "Extra Pilsner", 30, "beskrivelse");
		Produkt fadol_celebration = Controller.createProdukt(fadol, "Celebration", 30, "beskrivelse");
		Produkt fadol_blondie = Controller.createProdukt(fadol, "Blondie", 30, "beskrivelse");
		Produkt fadol_foraarsbryg = Controller.createProdukt(fadol, "Forårsbryg", 30, "beskrivelse");
		Produkt fadol_indiaPaleAle = Controller.createProdukt(fadol, "India Pale Ale", 30, "beskrivelse");
		Produkt fadol_julebryg = Controller.createProdukt(fadol, "Julebryg", 30, "beskrivelse");
		Produkt fadol_imperialStout = Controller.createProdukt(fadol, "Imperial Stout", 30, "beskrivelse");
		Produkt fadol_special = Controller.createProdukt(fadol, "Special", 30, "beskrivelse");

		ProduktKategori andreDrikke = Controller.createProduktKategori("Andre Drikke", Enhed.GLAS, false, 1);
		Produkt aeblebrus = Controller.createProdukt(andreDrikke, "Æblebrus", 15, "beskrivelse");
		Produkt cola = Controller.createProdukt(andreDrikke, "Cola", 15, "beskrivelse");
		Produkt nikoline = Controller.createProdukt(andreDrikke, "Nikoline", 15, "beskrivelse");
		Produkt sevenUp = Controller.createProdukt(andreDrikke, "7-Up", 15, "beskrivelse");
		Produkt vand = Controller.createProdukt(andreDrikke, "Vand", 10, "beskrivelse");

		ProduktKategori tilbehør = Controller.createProduktKategori("Tilbehør", Enhed.GRAM, false, 1);
		Produkt chips = Controller.createProdukt(tilbehør, "Chips", 10, "beskrivelse");
		Produkt peanuts = Controller.createProdukt(tilbehør, "Peanuts", 10, "beskrivelse");

		ProduktKategori spiritus = Controller.createProduktKategori("Spiritus", Enhed.LITER, false, 0);
		Produkt soAarhus = Controller.createProdukt(spiritus, "Spirit of Aarhus", 300, "beskrivelse");
		Produkt soaMedPind = Controller.createProdukt(spiritus, "SOA med pind", 350, "beskrivelse");
		Produkt whisky = Controller.createProdukt(spiritus, "Whiskey", 500, "beskrivelse");
		Produkt liqOfAarhus = Controller.createProdukt(spiritus, "Liquor of Aarhus", 175, "beskrivelse");

		ProduktKategori beklædning = Controller.createProduktKategori("Beklædning", Enhed.BEKLÆDNING, false, 0);
		Produkt tshirt = Controller.createProdukt(beklædning, "T-shirt", 70, "tøj");
		Produkt polo = Controller.createProdukt(beklædning, "Polo", 100, "tøj");
		Produkt cap = Controller.createProdukt(beklædning, "Cap", 30, "kasket");

		ProduktKategori fustage = Controller.createProduktKategori("fustage", Enhed.LITER, true, 0);
		fustage.setPant(true);
		fustage.setPantPris(200);
		Produkt fustage_klosterbryg = Controller.createProdukt(fustage, "Klostebryg 20 liter", 775, "b");
		Produkt fustage_jazzClassic = Controller.createProdukt(fustage, "JazzClassic 25 liter", 625, "b");
		Produkt fustage_extraPilsner = Controller.createProdukt(fustage, "ExtraPilsner 25 liter", 575, "b");
		Produkt fustage_celebration = Controller.createProdukt(fustage, "Celebration 20 liter", 775, "b");
		Produkt fustage_blondie = Controller.createProdukt(fustage, "Blondie 25 liter", 700, "b");
		Produkt fustage_foraarsbryg = Controller.createProdukt(fustage, "Foraarsbryg 20 liter", 775, "b");
		Produkt fustage_indiaPaleAle = Controller.createProdukt(fustage, "IndiaPaleAle 20 liter", 775, "b");
		Produkt fustage_julebryg = Controller.createProdukt(fustage, "julebryg 20 liter", 775, "b");
		Produkt fustage_imperialStout = Controller.createProdukt(fustage, "Imperial Stout 20 liter", 775, "b");

		ProduktKategori kulsyre = Controller.createProduktKategori("Kulsyre", Enhed.KG, true, 0);
		kulsyre.setPant(true);
		kulsyre.setPantPris(1000);
		Produkt seks_kg = Controller.createProdukt(kulsyre, "6 kg kulsyre", 400, "b");

		ProduktKategori malt = Controller.createProduktKategori("Malt", Enhed.KG, false, 0);
		Produkt sæk_25kg = Controller.createProdukt(malt, "sæk 25", 300, "b");

		ProduktKategori anlæg = Controller.createProduktKategori("Anlæg", Enhed.ANLÆG, true, 0);
		Produkt hane_1 = Controller.createProdukt(anlæg, "1-hane", 250, "b");
		Produkt haner_2 = Controller.createProdukt(anlæg, "2-haner", 400, "b");
		Produkt haner_flere = Controller.createProdukt(anlæg, "flere-haner", 500, "b");
		Produkt levering = Controller.createProdukt(anlæg, "levering", 500, "b");
		Produkt krus = Controller.createProdukt(anlæg, "Krus", 60, "b");

		ProduktKategori glas = Controller.createProduktKategori("Glas", Enhed.GLAS, false, 0);
		Produkt glas_alle = Controller.createProdukt(glas, "Glas", 15, "glas");

		// KlippeKort----------------------------------------------------------------------------------

		ProduktKategori klippeKortKategori = Controller.createProduktKategori("Klippekort", Enhed.KLIP, false, 0);
		Produkt Klippekort = Controller.createProdukt(klippeKortKategori, "KlippeKort", 100, "Klippekort");
		Klippekort.setIsKlippekort(true);

		// Salgssituation
		// ----------------------------------------------------------------------------
		Salgssituation fredagsbar = Controller.createSalgssituation("Fredagsbar");
		Salgssituation butik = Controller.createSalgssituation("Butik");

		fredagsbar.createProduktPris(klosterbryg, 50);
		fredagsbar.createProduktPris(SGB, 50);
		fredagsbar.createProduktPris(extraPilsner, 50);
		fredagsbar.createProduktPris(celebration, 50);
		fredagsbar.createProduktPris(blondie, 50);
		fredagsbar.createProduktPris(foraarsbryg, 50);
		fredagsbar.createProduktPris(indiaPaleAle, 50);
		fredagsbar.createProduktPris(julebryg, 50);
		fredagsbar.createProduktPris(juletonden, 50);
		fredagsbar.createProduktPris(oldStrongAle, 50);
		fredagsbar.createProduktPris(fregattenJylland, 50);
		fredagsbar.createProduktPris(imperialStout, 50);
		fredagsbar.createProduktPris(tribute, 50);
		fredagsbar.createProduktPris(blackMonster, 50);

		fredagsbar.createProduktPris(fadol_klosterbryg, 30);
		fredagsbar.createProduktPris(fadol_jazzClassic, 30);
		fredagsbar.createProduktPris(fadol_extraPilsner, 30);
		fredagsbar.createProduktPris(fadol_celebration, 30);
		fredagsbar.createProduktPris(fadol_blondie, 30);
		fredagsbar.createProduktPris(fadol_foraarsbryg, 30);
		fredagsbar.createProduktPris(fadol_indiaPaleAle, 30);
		fredagsbar.createProduktPris(fadol_julebryg, 30);
		fredagsbar.createProduktPris(fadol_imperialStout, 30);
		fredagsbar.createProduktPris(fadol_special, 30);

		fredagsbar.createProduktPris(aeblebrus, 15);
		fredagsbar.createProduktPris(cola, 15);
		fredagsbar.createProduktPris(nikoline, 15);
		fredagsbar.createProduktPris(sevenUp, 15);
		fredagsbar.createProduktPris(vand, 10);

		fredagsbar.createProduktPris(chips, 10);
		fredagsbar.createProduktPris(peanuts, 10);

		fredagsbar.createProduktPris(soAarhus, 300);
		fredagsbar.createProduktPris(soaMedPind, 350);
		fredagsbar.createProduktPris(whisky, 500);
		fredagsbar.createProduktPris(Klippekort, 100);

		butik.createProduktPris(klosterbryg, 36);
		butik.createProduktPris(SGB, 36);
		butik.createProduktPris(extraPilsner, 36);
		butik.createProduktPris(celebration, 36);
		butik.createProduktPris(blondie, 36);
		butik.createProduktPris(foraarsbryg, 36);
		butik.createProduktPris(indiaPaleAle, 36);
		butik.createProduktPris(julebryg, 36);
		butik.createProduktPris(juletonden, 36);
		butik.createProduktPris(oldStrongAle, 36);
		butik.createProduktPris(fregattenJylland, 36);
		butik.createProduktPris(imperialStout, 36);
		butik.createProduktPris(tribute, 36);
		butik.createProduktPris(blackMonster, 50);

		butik.createProduktPris(soAarhus, 300);
		butik.createProduktPris(soaMedPind, 350);
		butik.createProduktPris(whisky, 500);
		butik.createProduktPris(liqOfAarhus, 175);

		butik.createProduktPris(fustage_klosterbryg, 775);
		butik.createProduktPris(fustage_jazzClassic, 625);
		butik.createProduktPris(fustage_extraPilsner, 575);
		butik.createProduktPris(fustage_celebration, 775);
		butik.createProduktPris(fustage_blondie, 700);
		butik.createProduktPris(fustage_foraarsbryg, 775);
		butik.createProduktPris(fustage_indiaPaleAle, 775);
		butik.createProduktPris(fustage_julebryg, 775);
		butik.createProduktPris(fustage_imperialStout, 200);

		butik.createProduktPris(seks_kg, 400);

		butik.createProduktPris(sæk_25kg, 300);

		butik.createProduktPris(tshirt, 70);
		butik.createProduktPris(polo, 100);
		butik.createProduktPris(cap, 30);

		butik.createProduktPris(hane_1, 250);
		butik.createProduktPris(haner_2, 400);
		butik.createProduktPris(haner_flere, 500);
		butik.createProduktPris(levering, 500);
		butik.createProduktPris(krus, 60);

		butik.createProduktPris(glas_alle, 15);

		// Kunde
		Kunde michael = Controller.createKunde("Michael Hansen", "Albertsgade 43", "40789487", "Mhansen@hotmail.com");
		Kunde henrik = Controller.createKunde("Henrik Korsby", "Duevej 9", "60638362", "Henrikkorsby@hotmail.com");

		// Udlejning
		Udlejning michael_udlejning = Controller.createUdlejning(LocalDate.now(), LocalDate.now(), michael);
		Udlejning henrik_udlejning = Controller.createUdlejning(LocalDate.now(), LocalDate.now(), henrik);

		// Ordrelinjer til udlejningen
		Controller.createOrdreLinje(michael_udlejning, haner_flere, haner_flere.getNormalpris(), 1);
		Controller.createOrdreLinje(michael_udlejning, hane_1, hane_1.getNormalpris(), 1);
		Controller.createOrdreLinje(michael_udlejning, haner_2, haner_2.getNormalpris(), 1);
		Controller.createOrdreLinje(michael_udlejning, seks_kg, seks_kg.getNormalpris(), 1);

		Controller.createOrdreLinje(henrik_udlejning, haner_2, haner_2.getNormalpris(), 1);
		Controller.createOrdreLinje(henrik_udlejning, fustage_blondie, fustage_blondie.getNormalpris(), 1);

		Controller.saveStorage();
	}
}
