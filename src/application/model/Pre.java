/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package application.model;

public class Pre {
	public static void require(boolean condition) {
		if (!condition)
			throw new RuntimeException("Pre violated");
	}
}
