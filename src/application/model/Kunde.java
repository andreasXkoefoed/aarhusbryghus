/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package application.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;

public class Kunde implements Serializable {

	private int kundeID; // <--- nummer(1000) + 1 for hver nyoprettet kunde
	private static int nummer = 1000; // <--- Incrementere kundeID med +1
	private String navn;
	private String adresse;
	private String telefonNr;
	private String email;

	// Composition: Kunde 1 <>---- 0..* Udlejning
	private ArrayList<Udlejning> udlejninger = new ArrayList<>();

	/**
	 * Opretter en ny kunde med navn, adresse, telefonnr, email Note: Hver ny kunde
	 * får tildelt en kundeID med en incrementering på 1000 + 1
	 */
	public Kunde(String navn, String adresse, String telefonNr, String email) {
		nummer++;
		this.kundeID = nummer;
		this.navn = navn;
		this.adresse = adresse;
		this.telefonNr = telefonNr;
		this.email = email;
	}

	public int getKundeID() {
		return this.kundeID;
	}

	public String getNavn() {
		return this.navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public String getAdresse() {
		return this.adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTelefonNr() {
		return this.telefonNr;
	}

	public void setTelefonNr(String telefonNr) {
		this.telefonNr = telefonNr;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	// metoder -------------------------------------------------------------------------------------------

	/** Returnere de udlejninger som kunden har */
	public ArrayList<Udlejning> getUdlejninger() {
		return new ArrayList<>(this.udlejninger);
	}

	/**
	 * Kunden opretter en udlejning med start & slut dato Pre: Kunde må ikke være
	 * null Note: Udlejning er oprettet og gemt i udlejninger listen
	 */
	public Udlejning createUdlejning(LocalDate startDato, LocalDate slutDato) {
		Udlejning udlejning = new Udlejning(startDato, slutDato, this);
		this.udlejninger.add(udlejning);
		return udlejning;
	}

	/** Pre: Udlejningen er ikke forbundet til andre kunder */
	public void addUdlejning(Udlejning udlejning) {
		if (!this.udlejninger.contains(udlejning)) {
			this.udlejninger.add(udlejning);
		}
	}

	/** Hvis udlejningen findes, så bliver den fjernet fra listen */
	public void removeUdlejning(Udlejning udlejning) {
		if (this.udlejninger.contains(udlejning)) {
			this.udlejninger.remove(udlejning);
		}
	}

	// udprint -------------------------------------------------------------------------------------

	@Override
	public String toString() {
		return this.navn;
	}
}
