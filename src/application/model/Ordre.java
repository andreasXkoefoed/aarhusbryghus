/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package application.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Ordre implements Serializable {

	private static int nummer = 1000; // <--- Incrementere ordrenummer med +1
	private int ordrenummer; // <--- nummer(1000) + 1 for hver ny ordrelinje
	private LocalDateTime oprettelsesdato;
	private double rabat;
	private double samletPris;
	private boolean isBetalt;
	private int klipBrugt;
	private BetalingsType betalingstype; // <--- Enum klasse

	// Composition: Ordre 1 <>--- 0..* Ordrelinje
	private ArrayList<Ordrelinje> ordrelinjer = new ArrayList<>();

	/**
	 * Opretter en ny ordre med et ordrenummer og oprettelsesdato (samme dag)
	 */
	public Ordre() {
		nummer++;
		this.ordrenummer = nummer;
		this.oprettelsesdato = LocalDateTime.now();
	}

	public int getOrdrenummer() {
		return this.ordrenummer;
	}

	public LocalDateTime getOprettelsesdato() {
		return this.oprettelsesdato;
	}

	public double getRabat() {
		return this.rabat;
	}

	/** Note: Returnere rabatten som et decimaltal */
	public void setRabat(int rabat) {
		this.rabat = rabat * 0.01;
	}

	public double getSamletPris() {
		return this.samletPris;
	}

	public void setSamletPris(double samletPris) {
		this.samletPris = samletPris;
	}

	public boolean isBetalt() {
		return this.isBetalt;
	}

	public void setBetalt(boolean isBetalt) {
		this.isBetalt = isBetalt;
	}

	public BetalingsType getBetalingstype() {
		return this.betalingstype;
	}

	public void setBetalingsType(BetalingsType betalingstype) {
		this.betalingstype = betalingstype;
	}

	// Metoder -------------------------------------------------------------------------------------------

	/**
	 * Returnere samlet antal klip brugt i ordrens ordrelinjer. Note: Klipbrugt
	 * feltet er opdateret med antal klip brugt
	 */
	public int getKlipBrugt() {
		int brugt = 0;
		for (Ordrelinje o : this.ordrelinjer) {
			brugt += o.getProdukt().getProduktkategori().getKlipværdi();
			this.klipBrugt = brugt;
		}
		return this.klipBrugt;
	}

	public void setKlipBrugt(int klipBrugt) {
		this.klipBrugt = klipBrugt;
	}

	/** Returnere en kopi af ordrens ordrelinjer */
	public ArrayList<Ordrelinje> getOrdrelinjer() {
		return new ArrayList<>(this.ordrelinjer);
	}

	/** Udregner og returnere den samlede pris for hele ordren */
	public double beregnSamletPris() {
		double samletPris = 0.0;
		for (Ordrelinje ordrelinje : this.ordrelinjer) {
			samletPris += ordrelinje.udregnOrdrelinjeSamletPris();
		}
		return samletPris;
	}

	public void setOrdreTotalPris(Ordre ordre) {
		this.samletPris = ordre.beregnSamletPris();
	}

	/**
	 * Opretter en ny ordrelinje med pris og antal Pre: Produkt må ikke være null
	 * Note: Den nyoprettede ordrelinje er herefter gemt i ordrelinje listen
	 */
	public Ordrelinje createOrdrelinje(Produkt produkt, double pris, int antal) {
		Ordrelinje ordrelinje = new Ordrelinje(produkt, pris, antal);
		this.ordrelinjer.add(ordrelinje);
		return ordrelinje;
	}

	/** Hvis ordrelinjen findes, bliver den fjernet fra listen */
	public void removeOrdrelinje(Ordrelinje ordrelinje) {
		if (this.ordrelinjer.contains(ordrelinje)) {
			this.ordrelinjer.remove(ordrelinje);
		}
	}

	// udprint
	// -------------------------------------------------------------------------------------------

	@Override
	public String toString() {
		return "Butik ID: " + getOrdrenummer();
	}

}
