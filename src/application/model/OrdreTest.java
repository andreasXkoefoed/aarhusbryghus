/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package application.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class OrdreTest {

	@Test
	public void testBeregnSamletPris() {

		ProduktKategori kategori = new ProduktKategori("Dåse", Enhed.CL, false, 1);
		ProduktKategori kategoriØL = new ProduktKategori("Flakse", Enhed.CL, false, 2);
		Produkt produkt = new Produkt(kategori, "Fanta", 10, "Sodavand");
		Produkt produkØL = new Produkt(kategoriØL, "Pilsner", 36, "Øl");

		Ordre o1 = new Ordre();
		assertEquals(70, o1.createOrdrelinje(produkt, 10, 7).udregnOrdrelinjeSamletPris(), 0.1);

		Ordre o2 = new Ordre();
		assertEquals(144, o2.createOrdrelinje(produkØL, 36, 4).udregnOrdrelinjeSamletPris(), 0.01);

	}

}
