/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package application.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SalgssituationTest {

	@Test
	public void testSalgssituation() {

		Salgssituation salg1 = new Salgssituation("Butik");
		Salgssituation salg2 = new Salgssituation("FredagsBar");

		assertEquals("Butik", salg1.getNavn());
		assertEquals("FredagsBar", salg2.getNavn());

	}

	@Test
	public void testCreateProduktPris() {

		ProduktKategori kategori = new ProduktKategori("Flaske", Enhed.CL, false, 2);
		Produkt produkt = new Produkt(kategori, "Klosterbryg", 36, "ØL");
		Salgssituation salg = new Salgssituation("Butik");

		Prisliste pris1 = salg.createProduktPris(produkt, 100);

		assertEquals(100, pris1.getPris(), 0.01);
	}

	@Test
	public void testGetNavn() {
		Salgssituation salgssituation = new Salgssituation("Butik");

		assertEquals("Butik", salgssituation.getNavn());

	}

	@Test
	public void testSetNavn() {
		Salgssituation salgssituation = new Salgssituation("Fredagsbar");

		salgssituation.setNavn("Butik");

		assertEquals("Butik", salgssituation.getNavn());
	}

	@Test
	public void testAddProduktPriser() {
		Salgssituation salg = new Salgssituation("Butik");
		ProduktKategori kategori = new ProduktKategori("Flaske", Enhed.CL, false, 2);
		Produkt produkt = new Produkt(kategori, "Klosterbryg", 36, "ØL");
		Prisliste pris = new Prisliste(50, produkt);

		assertEquals(0, salg.getPrislister().size());
		salg.addProduktPris(pris);
		assertEquals(1, salg.getPrislister().size());
		assertTrue(salg.getPrislister().contains(pris));
	}

	@Test
	public void testRemoveProduktPriser() {
		Salgssituation salg = new Salgssituation("Butik");
		ProduktKategori kategori = new ProduktKategori("Flaske", Enhed.CL, false, 2);
		Produkt produkt = new Produkt(kategori, "Klosterbryg", 36, "ØL");
		Prisliste pris1 = new Prisliste(50, produkt);
		Prisliste pris2 = new Prisliste(100, produkt);
		Prisliste pris3 = new Prisliste(200, produkt);

		assertEquals(0, salg.getPrislister().size());
		salg.addProduktPris(pris1);
		salg.addProduktPris(pris2);
		salg.addProduktPris(pris3);
		assertTrue(salg.getPrislister().contains(pris1));
		assertTrue(salg.getPrislister().contains(pris2));
		assertTrue(salg.getPrislister().contains(pris3));

		assertEquals(3, salg.getPrislister().size());
		salg.removeProduktPris(pris3);

		assertTrue(salg.getPrislister().contains(pris1));
		assertTrue(salg.getPrislister().contains(pris2));

		assertEquals(2, salg.getPrislister().size());
		salg.removeProduktPris(pris2);

		assertTrue(salg.getPrislister().contains(pris1));

		assertEquals(1, salg.getPrislister().size());
		salg.removeProduktPris(pris1);

		assertEquals(0, salg.getPrislister().size());

	}

	@Test
	public void testUpdateProduktPris() {
		Salgssituation salg = new Salgssituation("Butik");
		ProduktKategori kategori = new ProduktKategori("Flaske", Enhed.CL, false, 2);
		Produkt produkt = new Produkt(kategori, "Klosterbryg", 36, "ØL");
		Prisliste pris1 = new Prisliste(50, produkt);

		salg.addProduktPris(pris1);
		assertTrue(salg.getPrislister().contains(pris1));
		assertEquals(1, salg.getPrislister().size());
		assertEquals(50, pris1.getPris(), 0.01);

		salg.updateProduktPris(pris1, 100);
		assertEquals(100, pris1.getPris(), 0.01);
	}

}
