/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package application.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Salgssituation implements Serializable {
	private String navn;

	// Composition: Salgssituation 1 <>--- 0..* ProduktPris
	private ArrayList<Prisliste> prislister = new ArrayList<>();

	/**
	 * Opretter en ny salgssituation med navn 
	 * Pre: Navn må ikke være null
	 */
	public Salgssituation(String navn) {
		this.navn = navn;
	}

	public String getNavn() {
		return this.navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	// Metoder -------------------------------------------------------------------------------------------

	/**
	 * Opretter en produktpris, med et produkt og en pris Pre: Produkt må ikke være
	 * null, pris >= 0 Note: Den nyoprettede produktpris er herefter gemt i
	 * produktpriser listen
	 */
	public Prisliste createProduktPris(Produkt produkt, double pris) {
		Prisliste produktpris = new Prisliste(pris, produkt);
		this.prislister.add(produktpris);
		return produktpris;
	}

	/**
	 * Tilføjer en produktpris til salgssituationen Pre: Produktprisen må ikke i
	 * forvejen findes i listen Note: produktprisen bliver tilføjet til
	 * produktpriser listen
	 */
	public void addProduktPris(Prisliste pris) {
		if (!this.prislister.contains(pris)) {
			this.prislister.add(pris);
		}
	}

	/**
	 * Fjerner salgssituationens produktpris fra listen, hvis den findes
	 */
	public void removeProduktPris(Prisliste pris) {
		if (this.prislister.contains(pris)) {
			this.prislister.remove(pris);
		}
	}

	/**
	 * Opdatere produktprisen med en ny pris Pre: Produktpris er ikke null, pris >=
	 * 0 Produktpris objektet har fået en ny pris
	 */
	public void updateProduktPris(Prisliste produktpris, double pris) {
		produktpris.setPris(pris);
	}

	/** Returnere en kopi af produktpriser listen */
	public ArrayList<Prisliste> getPrislister() {
		return new ArrayList<>(this.prislister);
	}

	// udprint -------------------------------------------------------------------------------------------

	@Override
	public String toString() {
		return this.navn;
	}

}
