/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package application.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

// Udlejning er en specialisering af Ordre
public class Udlejning extends Ordre implements Serializable {

	private LocalDate startDato;
	private LocalDate slutDato;
	private int pant;
	private boolean isPantBetalt;

	// Composition: Udlejning 1 <>--- 0..* Ordrelinje
	private List<Ordrelinje> afleveredeUdlejninger = new ArrayList<>();

	// Composition: Udlejning 0..* ---<> 1 Kunde
	private Kunde kunde;

	/**
	 * Opretter en ny udlejning med startdato, slutdato og med forbindelse til en
	 * kunde Pre: Kunde, startdato, slutdato må ikke være null
	 */
	public Udlejning(LocalDate startDato, LocalDate slutDato, Kunde kunde) {
		super();
		this.startDato = startDato;
		this.slutDato = slutDato;
		this.kunde = kunde;
	}

	public LocalDate getStartDato() {
		return this.startDato;
	}

	public LocalDate getSlutDato() {
		return this.slutDato;
	}

	public int getUdlejningID() {
		return super.getOrdrenummer();
	}

	public int getPant() {
		return this.pant;
	}

	public void setPant(int pant) {
		this.pant = pant;
	}

	public boolean isPantBetalt() {
		return this.isPantBetalt;
	}

	public void setPantBetalt(boolean pantStatus) {
		this.isPantBetalt = pantStatus;
	}

	@Override
	public boolean isBetalt() {
		return super.isBetalt();
	}

	@Override
	public void setBetalt(boolean isBetalt) {
		super.setBetalt(isBetalt);
	}

	// -------------------------------------------------------------------------------------------------

	/** Returnere kunde som er forbundet til udlejningen */
	public Kunde getKunde() {
		return this.kunde;
	}

	/** Returnere en kopi af de afleverede udlejninger */
	public List<Ordrelinje> getAfleveredeUdlejninger() {
		return new ArrayList<>(this.afleveredeUdlejninger);
	}

	/**
	 * Det udlejede produkt afleveres tilbage og ordrelinjen fjernes fra ordren,
	 * gennem super klassen Ordre Pre: Den findes ikke i forvejen i listen
	 */
	public void afleverUdlejedeProdukt(Ordrelinje ordrelinje) {
		if (!this.afleveredeUdlejninger.contains(ordrelinje)) {
			this.afleveredeUdlejninger.add(ordrelinje);
			super.removeOrdrelinje(ordrelinje);
		}

	}

	// Metoder -------------------------------------------------------------------------------------------

	/** Returnere udlejningens ordrelinjer, gennem super klassen Ordre */
	@Override
	public ArrayList<Ordrelinje> getOrdrelinjer() {
		return super.getOrdrelinjer();
	}

	/**
	 * Hvis ordrelinjen findes, fjernes den fra ordren, gennem super klassen Ordre
	 */
	@Override
	public void removeOrdrelinje(Ordrelinje ordrelinje) {
		super.removeOrdrelinje(ordrelinje);
	}

	/**
	 * Tilføjer den rigtige pant til hver ordrelinje Pre: Ordrelinje har en boolean
	 * true i pant Note: Pant feltet er opdateret med det samlede antal pant
	 */
	public void plusPant(Ordrelinje ordrelinje, boolean pant) {
		if (ordrelinje.getProdukt().getProduktkategori().isPant()) {
			if (pant) {
				this.pant += ordrelinje.getProdukt().getProduktkategori().getPantPris();
			} else {
				this.pant -= ordrelinje.getProdukt().getProduktkategori().getPantPris();
			}
		}
	}

	/**
	 * Returnere den samlede pris for de udlejede produkter som IKKE er brugt Pre:
	 * Ordrelinje har en boolean false i 'brugtUdlejetProdukt' i klassen Ordrelinje
	 */
	public double ikkeForbrugteProdukterPris() {
		double ikkeBrugtPris = 0.0;
		for (Ordrelinje ordrelinje : this.afleveredeUdlejninger) {
			if (!ordrelinje.isBrugtUdlejetProdukt()) {
				ikkeBrugtPris += ordrelinje.getProdukt().getNormalpris();
			}
		}
		return ikkeBrugtPris;
	}

	/**
	 * Udregner og returnere den totale pris for hele ordren Pre: Udlejnings ordren
	 * har en boolean false i isPantBetalt, dvs. at panten ER betalt Note:
	 */
	public double totalPris() {
		double totalPris = 0.0;
		if (!isPantBetalt()) {
			totalPris = (super.getSamletPris() + getPant()) - ikkeForbrugteProdukterPris() - getPant();
		}
		return totalPris;
	}

	// udskrift --------------------------------------------------------------------------------------------

	@Override
	public String toString() {
		return "Udlejning ID: " + getUdlejningID();
	}
}