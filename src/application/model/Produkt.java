/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package application.model;

import java.io.Serializable;

public class Produkt implements Serializable {

	private String navn;
	private double normalpris;
	private String beskrivelse;
	private boolean isKlippekort;

	// Aggregering: 0..* ---<> 1 ProduktKategori
	private ProduktKategori produktkategori;

	/**
	 * Opretter et nyt produkt med en produktkategori forbindelse, navn, normalpris
	 * og beskrivelse Pre: ProduktKategori og navn må ikke være null, normalpris >= 0
	 */
	public Produkt(ProduktKategori produktkategori, String navn, double normalpris, String beskrivelse) {
		this.navn = navn;
		this.normalpris = normalpris;
		this.produktkategori = produktkategori;
		this.beskrivelse = beskrivelse;
	}

	public String getNavn() {
		return this.navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public double getNormalpris() {
		return this.normalpris;
	}

	public void setNormalpris(double normalpris) {
		this.normalpris = normalpris;
	}

	public String getBeskrivelse() {
		return this.beskrivelse;
	}

	public void setBeskrivelse(String beskrivelse) {
		this.beskrivelse = beskrivelse;
	}

	public Boolean getIsKlippekort() {
		return this.isKlippekort;
	}

	public void setIsKlippekort(Boolean isKlippekort) {
		this.isKlippekort = isKlippekort;
	}

	// metoder  -------------------------------------------------------------------------------------------

	public ProduktKategori getProduktkategori() {
		return this.produktkategori;
	}

	/**
	 * Produktet forbinder til en kategori Pre: ProduktKategori må ikke være null
	 * Note: ProduktKategori forbinder med produktet i dennes addProdukt metode
	 */
	public void setProduktKategori(ProduktKategori produktkategori) {
		if (this.produktkategori != produktkategori) {
			Pre.require(this.produktkategori == null);
			this.produktkategori = produktkategori;
			produktkategori.addProdukt(this);
		}
	}

	// udprint
	// ------------------------------------------------------------------------------------

	@Override
	public String toString() {
		return this.navn + " " + this.normalpris;
	}
}
