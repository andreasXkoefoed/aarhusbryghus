/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package application.model;

public enum BetalingsType {
	DANKORT, MOBILEPAY, KONTANT, KLIPPEKORT, REGNING;
}
