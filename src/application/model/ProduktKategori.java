/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package application.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ProduktKategori implements Serializable {

	private String navn;
	private Enhed enhed; // <--- Enum klasse
	private int klipværdi;
	private int pantPris;
	private boolean isUdlejning = false;
	private boolean isPant = false;

	// Aggregering: ProduktKategori 1 <>--- 0..* Produkt
	private ArrayList<Produkt> produkter = new ArrayList<>();

	/**
	 * Opretter en produktKategori med navn, enhed, isUdlejning(false) og en klipværdi
	 * Pre: Navn og enhed må ikke være null
	 * */
	public ProduktKategori(String navn, Enhed enhed, boolean isUdlejning, int klipværdi) {
		this.navn = navn;
		this.enhed = enhed;
		this.isUdlejning = isUdlejning;
		this.klipværdi = klipværdi;
	}

	public String getNavn() {
		return this.navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public Enhed getEnhed() {
		return this.enhed;
	}

	public void setEnhed(Enhed enhed) {
		this.enhed = enhed;
	}

	public int getKlipværdi() {
		return this.klipværdi;
	}

	public int getPantPris() {
		return this.pantPris;
	}

	public void setPantPris(int pantPris) {
		this.pantPris = pantPris;
	}

	public boolean isUdlejning() {
		return this.isUdlejning;
	}

	public void setUdlejning(boolean isUdlejning) {
		this.isUdlejning = isUdlejning;
	}

	public boolean isPant() {
		return this.isPant;
	}

	public void setPant(boolean isPant) {
		this.isPant = isPant;
	}

	// metoder -------------------------------------------------------------------------------------------

	/** Returnere en kopi af produkter som liste */
	public ArrayList<Produkt> getProdukter() {
		return new ArrayList<>(this.produkter);
	}

	/**
	 * Tilføjer et produkt i produktkategoriens liste
	 * Pre: Produktet må ikke i forvejen findes i listen
	 * Note: Produktet forbinder samtidig denne produktkategori som dennes produktkategori */
	public void addProdukt(Produkt produkt) {
		if (!this.produkter.contains(produkt)) {
			this.produkter.add(produkt);
			produkt.setProduktKategori(this);
		}
	}

	/** Hvis produktet findes, bliver den fjernet fra listen */
	public void removeProdukt(Produkt produkt) {
		if (this.produkter.contains(produkt)) {
			this.produkter.remove(produkt);
		}
	}

	// udprint ------------------------------------------------------------------------------------

	@Override
	public String toString() {
		return this.navn;
	}

}
