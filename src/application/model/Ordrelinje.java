/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package application.model;

import java.io.Serializable;

public class Ordrelinje implements Serializable {
	private int ordrelinjenummer; // <--- nummer(1) + 1 for hver ny ordrelinje
	private int nummer = 1; // <--- Incrementere ordrelinjenummer med +1
	private int antal;
	private boolean brugtUdlejetProdukt;

	// Må vi vente med at definere
	private Produkt produkt;

	// Associering: Ordrelinje 0..* ---> 1 ProduktPris
	private double produktPris;

	/**
	 * Opretter en ny ordrelinje med et produkt, produktpris og antal Pre: Produkt
	 * må ikke være null, produktpris >= 0, antal >= 0
	 */
	public Ordrelinje(Produkt produkt, double produktpris, int antal) {
		this.nummer++;
		this.ordrelinjenummer = this.nummer;
		this.antal = antal;
		this.produkt = produkt;
		this.produktPris = produktpris;
	}

	public int getOrdrelinjenummer() {
		return this.ordrelinjenummer;
	}

	public int getAntal() {
		return this.antal;
	}

	public void setAntal(int antal) {
		this.antal = antal;
	}

	public boolean isBrugtUdlejetProdukt() {
		return this.brugtUdlejetProdukt;
	}

	public void setBrugtUdlejetProdukt(boolean brugtUdlejetProdukt) {
		this.brugtUdlejetProdukt = brugtUdlejetProdukt;
	}

	// -------------------------------------------------------------------------------------------------------

	public Produkt getProdukt() {
		return this.produkt;
	}

	public double getProduktPris() {
		return this.produktPris;
	}

	// Metoder
	// ------------------------------------------------------------------------------------------------

	/** Returnere udlejningens ordrelinjer, gennem super klassen Ordre */
	public double udregnOrdrelinjeSamletPris() {
		return this.produktPris * this.antal;
	}

	// udprint
	// -------------------------------------------------------------------------------------------------

	@Override
	public String toString() {
		return this.antal + " x " + this.produkt.getNavn() + " | Pris: " + udregnOrdrelinjeSamletPris() + " kr,-";
	}
}
