/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package application.model;

import java.io.Serializable;

public class Prisliste implements Serializable {
	private double pris;

	// Associering: ProduktPris 0..* <----> 1 Produkt
	private Produkt produkt;

	/**
	 * Opretter en ny produktPris med et allerede eksisterende produkt objekt og en
	 * pris Pre: Produkt må ikke være null, pris >= 0
	 */
	public Prisliste(double pris, Produkt produkt) {
		this.pris = pris;
		this.produkt = produkt;
	}

	public double getPris() {
		return this.pris;
	}

	public void setPris(double pris) {
		this.pris = pris;
	}

	// -------------------------------------------------------

	public Produkt getProdukt() {
		return this.produkt;
	}

	public void setProdukt(Produkt produkt) {
		this.produkt = produkt;
	}

	// udprint -----------------------------------------------

	@Override
	public String toString() {
		return this.produkt.getNavn() + " " + this.pris;
	}
}
