/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package application.model;

public enum Enhed {
	CL, LITER, KG, GLAS, GRAM, BEKLÆDNING, ANLÆG, KLIP;
}
