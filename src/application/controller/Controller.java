/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */

package application.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import application.model.BetalingsType;
import application.model.Enhed;
import application.model.Kunde;
import application.model.Ordre;
import application.model.Ordrelinje;
import application.model.Prisliste;
import application.model.Produkt;
import application.model.ProduktKategori;
import application.model.Salgssituation;
import application.model.Udlejning;
import storage.Storage;

public abstract class Controller {

	/**
	 * Opretter en ny produktkategori Pre: navn, enhed, idUdlejning, klipværdi må
	 * ikke være null, klipværdi >= 0 Note: ProduktKategori objektet bliver gemt i
	 * storage
	 * 
	 * @return
	 */
	public static ProduktKategori createProduktKategori(String navn, Enhed enhed, boolean isUdlejning, int klipværdi) {
		ProduktKategori kategori = new ProduktKategori(navn, enhed, isUdlejning, klipværdi);
		Storage.getInstance().storeProduktKategori(kategori);
		return kategori;
	}

	/**
	 * Sletter produktkategori objektet fra storage
	 * 
	 * @param produktkategori
	 */
	public static void deleteProduktKategori(ProduktKategori produktkategori) {
		Storage.getInstance().deleteProduktKategori(produktkategori);
	}

	/**
	 * Opdatere produktkategori objektet, med navn og enhed Pre: Produktkategori,
	 * navn og enhed må ikke være null
	 */
	public static void updateProduktKategori(ProduktKategori produktkategori, String navn, Enhed enhed) {
		produktkategori.setNavn(navn);
		produktkategori.setEnhed(enhed);
	}

	/**
	 * Returnere alle produktkategorier fra storage
	 * 
	 * @return
	 */
	public static ArrayList<ProduktKategori> getProduktKategorier() {
		return Storage.getInstance().getKategorier();
	}

	// -------------------------------------------- Produkt --------------------------------------------------\\

	// createProdukt, deleteProdukt, getKategoriensProdukter, getProduktsKategori,
	// updateProdukt, getProdukter

	/**
	 * Opretter et nyt produkt med en produktkategori, navn, pris og beskrivelse
	 * Pre: Produktkategori, navn og pris må ikke være null, pris >= 0 Note: Produkt
	 * objektet bliver gemt i storage
	 * 
	 * @return
	 */
	public static Produkt createProdukt(ProduktKategori kategori, String navn, double pris, String beskrivelse) {
		Produkt produkt = new Produkt(kategori, navn, pris, beskrivelse);
		kategori.addProdukt(produkt);
		Storage.getInstance().storeProdukt(produkt);
		return produkt;
	}

	/**
	 * Sletter et givent produkt fra en kategori Pre: Produktkategori, produkt må
	 * ikke være null Note: Produktet er fjernet fra kategorien og fra storage
	 */
	public static void deleteProdukt(ProduktKategori kategori, Produkt produkt) {
		kategori.removeProdukt(produkt);
		Storage.getInstance().deleteProdukt(produkt);
	}

	/**
	 * Returnere alle produkter som findes i produktkategorien
	 * 
	 * @return
	 */
	public static ArrayList<Produkt> getKategoriensProdukter(ProduktKategori kategori) {
		return kategori.getProdukter();
	}

	/**
	 * Returnere alle produktkategorier for en given salgssituation Pre:
	 * Salgssituation må ikke være null
	 * 
	 * @return
	 */
	public static Set<ProduktKategori> getProduktsKategori(Salgssituation salgssituation) {
		Set<ProduktKategori> kategori = new HashSet<>();
		for (int i = 0; i < salgssituation.getPrislister().size(); i++) {
			if (!salgssituation.getPrislister().get(i).getProdukt().getProduktkategori().isUdlejning()) {
				kategori.add(salgssituation.getPrislister().get(i).getProdukt().getProduktkategori());
			}
		}
		return kategori;
	}

	/**
	 * Opdatere et givent produkt med navn og normalpris Pre: Produkt, navn og
	 * normalpris må ikke være null, normalpris >= 0 Produktet har fået ændret navn
	 * og normalpris
	 */
	public static void updateProdukt(Produkt produkt, String navn, int normalpris, String beskrivelse) {
		produkt.setNavn(navn);
		produkt.setNormalpris(normalpris);
		produkt.setBeskrivelse(beskrivelse);
	}

	/**
	 * Returnere alle produkter fra storage
	 * 
	 * @return
	 */
	public static ArrayList<Produkt> getProdukter() {
		return Storage.getInstance().getProdukter();
	}

	// --------------------------------------- Salgssituation---------------------------------------------------------

	// createSalgssiation, createProduktPris, updateProduktPris, getProduktPrisOnSalgssituation
	// getSalgssituationer, getProduktPris, setKlippekort, setKlipBrugt \\

	/**
	 * Opretter en salgssituation med et navn Pre: Navn må ikke være null Note:
	 * Salgssituation objektet bliver gemt i storage
	 * 
	 * @return
	 */
	public static Salgssituation createSalgssituation(String navn) {
		Salgssituation salgssituationen = new Salgssituation(navn);
		Storage.getInstance().storeSalgssituation(salgssituationen);
		return salgssituationen;
	}

	/**
	 * Opretter en produktpris for en salgssituation, til et produkt med en valgt
	 * pris Pre: Salgssituation, produkt og pris må ikke være null, pris >= 0
	 * 
	 * @return
	 */
	public static Prisliste createProduktPris(Salgssituation salgssituation, Produkt produkt, int pris) {
		return salgssituation.createProduktPris(produkt, pris);
	}

	/**
	 * Opdatere produkt og pris for en given salgssituation Pre: Salgssituation,
	 * produktpris, pris må ikke være null, pris >= 0
	 */
	public static void updateProduktPris(Salgssituation salgssituation, Prisliste produktpris, int pris) {
		salgssituation.updateProduktPris(produktpris, pris);
	}

	/**
	 * Returnere produkterne for en given salgssituation
	 * 
	 * @return
	 */
	public static ArrayList<Prisliste> getProduktprisOnSalgssituation(Salgssituation salgssituation) {
		return salgssituation.getPrislister();
	}

	/**
	 * Returnere alle salgssituationer
	 * 
	 * @return
	 */
	public static ArrayList<Salgssituation> getSalgssituationer() {
		return Storage.getInstance().getSalgssituation();
	}

	/**
	 * Sætter et true/false værdi på et givent produkt om det kan bruges som
	 * klippekort eller ikke Pre: produkt må ikke være null
	 */
	public void setKlippeKort(Produkt produkt, Boolean isKlippekort) {
		produkt.setIsKlippekort(isKlippekort);
	}

	/**
	 * Opdatere for ordren antal klip brugt Pre: antal < 0
	 */
	public void setklipbrugt(Ordre ordre, int antal) {
		if (antal < 0) {
			throw new IllegalArgumentException("Skal være over 0");
		} else {
			ordre.setKlipBrugt(antal);
		}
	}

	// -------------------------------------------- Ordre -----------------------------------------------\\

	// createOrdre, getOrdre, removeOrdre, removeOrdrelinjeFromOrdre,
	// udregnPrisMedRabat
	// getOrdreTotalPris, samletAntalKlip, setBetalingsMetode, setBetalt \\

	/**
	 * Opretter en ordre Note: Ordre objektet bliver gemt i storage
	 * 
	 * @return
	 */
	public static Ordre createOrdre() {
		Ordre ordre = new Ordre();
		Storage.getInstance().storeOrdre(ordre);
		return ordre;
	}

	/**
	 * Returnere alle ordre fra storage
	 * 
	 * @return
	 */
	public static ArrayList<Ordre> getOrdre() {
		return Storage.getInstance().getOrdre();
	}

	/**
	 * Sletter en ordre fra storage
	 */
	public static void removeOrdre(Ordre ordre) {
		Storage.getInstance().deleteOrdre(ordre);
	}

	/**
	 * Fjerner en ordrelinje fra en ordre Pre: ordrelinje må ikke være null
	 */
	public static void removeOrdreLinjeFromOrdre(Ordre ordre, Ordrelinje ordrelinje) {
		ordre.removeOrdrelinje(ordrelinje);
	}

	/**
	 * Udregner rabatten i procent fra den samlede pris og returnere den samlede
	 * pris som double Note: Ordre opdatere samletPris med (samlet pris - rabat)
	 * 
	 * @return
	 */
	public static double udregnPrisMedRabat(Ordre ordre) {
		double samletPrisMedRabat;
		double procent = ordre.getRabat();
		samletPrisMedRabat = ordre.getSamletPris() * procent;
		samletPrisMedRabat = ordre.getSamletPris() - samletPrisMedRabat;
		ordre.setSamletPris(samletPrisMedRabat);

		return samletPrisMedRabat;
	}

	/**
	 * Returnere den total pris på ordren
	 * 
	 * @return
	 */
	public static double getOrdreTotalPris(Ordre ordre) {
		return ordre.beregnSamletPris();
	}

	/**
	 * Returnere det samlede antal klip på ordren
	 * 
	 * @return
	 */
	public static int samletAntalKlip(Ordre ordre) {
		return ordre.getKlipBrugt();
	}

	/**
	 * Sætter betalingstypen på en ordre, gennem brug af Enum klassen Note: Ordre
	 * sætter betalingstypen
	 */
	public static void setBetalingsMetode(Ordre ordre, BetalingsType betalingstype) {
		ordre.setBetalingsType(betalingstype);
	}

	/**
	 * Sætter ordren som værende betalt 
	 * Note: Ordre sætter en true værdi at denne er betalt
	 */
	public static void setBetalt(Ordre ordre) {
		ordre.isBetalt();
	}

	// -------------------------------------------- Ordrelinje -----------------------------------------------\\

	// createOrdrelinje, updateOrdrelinje

	/**
	 * Ordren opretter en ny ordrelinje
	 * Pre: Ordre må ikke være null
	 * Note: En ordrelinje er oprettet og tilføjet til ordren
	 */
	public static Ordrelinje createOrdreLinje(Ordre ordre, Produkt produkt, double pris, int antal) {
		Ordrelinje ordrelinje = ordre.createOrdrelinje(produkt, pris, antal);
		return ordrelinje;
	}

	/**
	 * Opdatere en ordrelinje
	 * Pre: Ordrelinje og produkt må ikke være null
	 * Note: Det valgte produkt på den valgte ordrelinje har fået ændret værdierne
	 * @return
	 */
	public static Ordrelinje updateOrdrelinje(Ordrelinje ordrelinje, Produkt produkt, double pris, int antal) {
		ordrelinje.setAntal(antal);
		return ordrelinje;
	}

	// -------------------------------------------- Udlejning -----------------------------------------------\\

	// createUdlejning, getOrdrelinjer, getUdlejningKategori, removeUdlejning
	// returnereUdlejedeProdukter, getReturneredeUdlejedeProdukter

	/**
	 * Kunden opretter en udlejning
	 * Kunde må ikke være null
	 * Udlejningen er oprettet og gemt i storage
	 * @return
	 */
	public static Udlejning createUdlejning(LocalDate startDato, LocalDate slutDato, Kunde kunde) {
		Udlejning udlejning = kunde.createUdlejning(startDato, slutDato);
		Storage.getInstance().storeOrdre(udlejning);
		return udlejning;
	}

	/**
	 * Fjerner en ordrelinje fra en valgt udlejning
	 * Pre: Udlejning må ikke være null
	 * Note: Ordrelinjen er fjernet fra udlejning og udlejningens pant er sat til værdien 0
	 */
	public static void removeOrdrelinjerFromUdlejning(Udlejning udlejning) {
		for (Ordrelinje ordrelinje : udlejning.getOrdrelinjer()) {
			udlejning.removeOrdrelinje(ordrelinje);
			udlejning.setPant(0);
		}
	}

	/**
	 * Returnere kundens ordrelinjer
	 * Pre: Kunde må ikke være null
	 * @return
	 */
	public static ArrayList<Ordrelinje> getOrdrelinjer(Kunde kunde) {
		ArrayList<Ordrelinje> ordrelinjer = new ArrayList<>();
		for (Udlejning ud : kunde.getUdlejninger()) {
			ordrelinjer.addAll(ud.getOrdrelinjer());
		}
		return ordrelinjer;
	}

	/**
	 * Returnere udlejningens produktkategori 
	 * Note: Hvis kategorien kan udlejes, så returneres den
	 * @return
	 */
	public static ArrayList<ProduktKategori> getUdlejningKategori() {
		ArrayList<ProduktKategori> udlejninger = new ArrayList<>();
		for (ProduktKategori k : Controller.getProduktKategorier()) {
			if (k.isUdlejning()) {
				udlejninger.add(k);
			}
		}
		return udlejninger;
	}

	/**
	 * Fjerne en udlejning fra en ordre
	 * Note: Storage fjerner udlejningen fra ordren
	 */
	public static void removeUdlejning(Udlejning udlejning) {
		Storage.getInstance().deleteOrdre(udlejning);
	}

	/**
	 * Gør det muligt at returnere udlejede produkter
	 * Pre: Udlejning og ordrelinje må ikke være null
	 * Note: Storage gemmer de afleverede udlejede produkter
	 */
	public static void returnereUdlejedeProdukter(Udlejning udlejning, Ordrelinje ordrelinje) {
		udlejning.afleverUdlejedeProdukt(ordrelinje);
		Storage.getInstance().storeAfleveredeUdlejedeProdukter(ordrelinje);
	}

	/**
	 * Returnere kundens afleverede udlejedede produkter
	 * Note: Storage indhenter de afleverede
	 * @return
	 */
	public static ArrayList<Ordrelinje> getReturneredeUdlejedeProdukter(Kunde kunde) {
		return Storage.getInstance().getAfleverede();
	}

	// -------------------------------------------- Kunde -----------------------------------------------\\

	// createKunde, getKunder

	/**
	 * Opretter en ny kunde
	 * Pre: Navn, adresse, telefonNr, email må ikke være null
	 * Note: Storage gemmer kunden
	 * @return
	 */
	public static Kunde createKunde(String navn, String adresse, String telefonNr, String email) {
		Kunde kunde = new Kunde(navn, adresse, telefonNr, email);
		Storage.getInstance().storeKunde(kunde);
		return kunde;
	}

	/**
	 * Returnere kunderne
	 * Note: Storage henter kunderne
	 */
	public static ArrayList<Kunde> getKunder() {
		return Storage.getInstance().getKunder();
	}

	// -------------------------------------------- Statestik -----------------------------------------------\\

	// getDagensSalg, betalteOrdreIPeriode, klipSoldPeriode, klipBrugtPeriode \\

	/**
	 * Returnere dagens salg
	 * Pre: LocalDate må ikke være null
	 * Note: For hver ordre, hvis ordren er betalt - beregn samlet pris
	 * @return
	 */
	public static double getDagensSalg(LocalDate localDate) {
		double samletpris = 0;
		for (Ordre ordre : Controller.getOrdre()) {
			if (ordre.isBetalt()) {
				samletpris += ordre.beregnSamletPris();
			}
		}
		return samletpris;
	}

	/**
	 * Returnere betalte ordre i en given periode
	 * Pre: startdato og slutdato må ikke være null
	 * Note: Hvis ordren er betalt, og ordren er imellem de 2 datoer, så returnere orden
	 * @return
	 */
	public static ArrayList<Ordre> BetalteOrdreIPeriode(LocalDate startdato, LocalDate slutdato) {
		ArrayList<Ordre> ordrer = new ArrayList<>();
		for (Ordre ordreBetalt : Storage.getInstance().getOrdre()) {
			if (ordreBetalt.getOprettelsesdato().toLocalDate().isAfter(startdato.minusDays(1))
					&& ordreBetalt.getOprettelsesdato().toLocalDate().isBefore(slutdato.plusDays(1))) {
				if (ordreBetalt.isBetalt()) {
					ordrer.add(ordreBetalt);
				}

			}
		}
		return ordrer;
	}

	/**
	 * Returnere antal solgte klip i en given periode
	 * Pre: startdato og slutdato må ikke være null
	 * Note: Hvis ordren er inden for en given dato og hvis ordren er betalt og 
	 * hvis produkternes kategori er KLIP, så for returnere samlede klip
	 * @return
	 */
	public static int KlipSoldPeriode(LocalDate startdato, LocalDate slutdato) {
		int sold = 0;
		for (Ordre ordre : Storage.getInstance().getOrdre()) {
			if (ordre.getOprettelsesdato().toLocalDate().isAfter(startdato.minusDays(1))
					&& ordre.getOprettelsesdato().toLocalDate().isBefore(slutdato.plusDays(1))) {
				if (ordre.isBetalt()) {
					for (Ordrelinje ol : ordre.getOrdrelinjer()) {
						if (ol.getProdukt().getProduktkategori().equals(getProduktKategorier().get(11))) {
							sold += ol.getAntal();
						}
					}

				}
			}
		}

		return sold;
	}

	/**
	 * Returnere antal klip brugt i en given periode
	 * Pre: startdato og slutdato må ikke være null
	 * Note: Hvis ordren er indenfor en given metode og ordren er betalt og 
	 * hvis betalingstypen er KLIPPEKORT, så returnere antal klip brugt for ordren
	 * @return
	 */
	public static int KlipBrugtPeriode(LocalDate startdato, LocalDate slutdato) {
		int brugt = 0;
		for (Ordre ordre : Storage.getInstance().getOrdre()) {
			if (ordre.getOprettelsesdato().toLocalDate().isAfter(startdato.minusDays(1))
					&& ordre.getOprettelsesdato().toLocalDate().isBefore(slutdato.plusDays(1))) {
				if (ordre.isBetalt()) {
					if (ordre.getBetalingstype() == BetalingsType.KLIPPEKORT) {
						brugt += ordre.getKlipBrugt();
					}
				}
			}
		}
		return brugt;

	}

	// -------------------------------------------- Storage--------------------------------------------------------

	public static void loadStorage() {
		Storage.getInstance().load();
	}

	public static void saveStorage() {
		Storage.getInstance().save();
	}

	public static void clearStorage() {
		Storage.getInstance().clear();
	}

}