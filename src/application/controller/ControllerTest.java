/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package application.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import application.model.Enhed;
import application.model.ProduktKategori;

public class ControllerTest {

	@Test
	public void testGetProduktKat() {
		ProduktKategori kategori1 = new ProduktKategori("Flaske", Enhed.CL, false, 2);
		ProduktKategori kategori2 = new ProduktKategori("Flaske", Enhed.CL, false, 2);
		ProduktKategori kategori3 = new ProduktKategori("Flaske", Enhed.CL, false, 2);
		Set<ProduktKategori> kategoris = new HashSet<>();

		assertEquals(0, kategoris.size());
		kategoris.add(kategori1);

		assertEquals(1, kategoris.size());
		kategoris.add(kategori1);
		assertEquals(1, kategoris.size());
		kategoris.add(kategori2);

		assertEquals(2, kategoris.size());
		kategoris.add(kategori2);
		assertEquals(2, kategoris.size());

		kategoris.add(kategori3);
		assertEquals(3, kategoris.size());

		assertTrue(kategoris.contains(kategori1));
		assertTrue(kategoris.contains(kategori2));
		assertTrue(kategoris.contains(kategori3));
	}

}
