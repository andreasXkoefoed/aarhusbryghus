/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package guiFx;

import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import application.controller.Controller;
import application.model.Kunde;
import application.model.Ordre;
import application.model.Ordrelinje;
import application.model.Produkt;
import application.model.ProduktKategori;
import application.model.Udlejning;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class UdlejningTab extends GridPane {

	// --------------------------------------------------------------------
	private ListView<Produkt> lvwProdukt = new ListView<Produkt>();
	private ListView<ProduktKategori> lvwProduktKategori = new ListView<ProduktKategori>();
	private ListView<Ordrelinje> lvwKurv = new ListView<>();
	private Button btnTømKurv, btnTilføj, btnFjern, btnOpretKunde, btnUdlej, btnVisUdlejninger;
	private TextField txfProduktNavn, txfAntal, txfNavn, txfAdresse, txfTelefonnummer, txfEmail;
	private TextArea txaSamletPris = new TextArea();
	private Label lblNavnKunde, lblAdresse, lblTelefonnummer, lblEmail, lblStartUdlejning,
			lblSlutUdlejning, lblKurv,
			lblSamletPris, lblFejl;
	private DatePicker startDato, slutDato;

	// Ny ordre oprettet ved start
	Ordre ordre = Controller.createOrdre();

	Kunde kunde = null;
	Udlejning udlejning = null;
	Ordrelinje ordrelinje = null;
	boolean pantBetaling = true;

	public UdlejningTab() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);

		// Overskrift 
		Label lblUdlejningText = new Label("Udlejning for Aarhus Bryghus");
		this.add(lblUdlejningText, 0, 0, 5, 1);
		lblUdlejningText.setFont(new Font(20));
		lblUdlejningText.setPrefWidth(300);

		// Række 0
		Label lblProduktKategori = new Label("Produkt Kategori: ");
		this.add(lblProduktKategori, 3, 2);
		this.add(this.lvwProduktKategori, 3, 3, 1, 2);
		this.lvwProduktKategori.setMaxHeight(80);
		this.lvwProduktKategori.getItems().setAll(Controller.getUdlejningKategori());

		Label lblProdukt = new Label("Produkt: ");
		this.add(lblProdukt, 3, 5);
		this.add(this.lvwProdukt, 3, 6, 1, 7);
		this.lvwProdukt.getItems().setAll(Controller.getUdlejningKategori().get(0).getProdukter());

		this.lvwProduktKategori.getSelectionModel().selectedIndexProperty()
				.addListener((p, ov, nv) -> this.selectedProduktKategoriChanged());

		this.lvwProdukt.getSelectionModel().selectedItemProperty()
				.addListener((p, ov, nv) -> this.selectedProdukterChanged());

		this.lblSamletPris = new Label("Samlet pris: ");
		this.add(this.lblSamletPris, 3, 13);
		this.txaSamletPris = new TextArea();
		this.txaSamletPris.setEditable(false);
		this.txaSamletPris.setPrefSize(50, 100);
		this.add(this.txaSamletPris, 3, 14);
		this.txaSamletPris.setFont(new Font(15));

		// Række 2
		Label lblValgtProdukt = new Label("Valgt Produkt: ");
		this.add(lblValgtProdukt, 4, 2);

		this.txfProduktNavn = new TextField();
		this.add(this.txfProduktNavn, 4, 3);
		this.txfProduktNavn.setMaxWidth(200);
		this.txfProduktNavn.setEditable(false);
		this.txfProduktNavn.setPromptText("Produkt navn");

		this.txfAntal = new TextField();
		this.txfAntal.setMaxWidth(50);
		this.txfAntal.setEditable(false);
		this.txfAntal.setPromptText("Antal");
		this.txfAntal.setText("1");

		this.btnTømKurv = new Button("Tøm kurv");
		this.add(this.btnTømKurv, 5, 3);
		this.btnTømKurv.setPrefSize(200, 25);
		this.btnTømKurv.setOnAction(plus -> tømKurvAction());

		this.btnTilføj = new Button("Tilføj");
		this.add(this.btnTilføj, 4, 4);
		this.btnTilføj.setPrefSize(200, 50);
		this.btnTilføj.setOnAction(event -> tilføjTilKurvAction());

		this.btnFjern = new Button("Fjern");
		this.add(this.btnFjern, 5, 4, 3, 1);
		this.btnFjern.setPrefSize(200, 50);
		this.btnFjern.setOnAction(event -> fjernFraKurvAction());

		this.lblKurv = new Label("Kurv");
		this.add(this.lblKurv, 4, 5);
		this.add(this.lvwKurv, 4, 6, 4, 7);

		this.btnUdlej = new Button("Udlej");
		this.add(this.btnUdlej, 4, 14, 4, 1);
		this.btnUdlej.setPrefSize(350, 100);
		this.btnUdlej.setOnAction(event -> opretUdlejningAction());

		// Række 3
		this.lblFejl = new Label();
		this.add(this.lblFejl, 4, 1, 2, 1);
		this.lblFejl.setTextFill(Color.RED);

		this.btnVisUdlejninger = new Button("Vis eksisterende udlejninger");
		this.add(this.btnVisUdlejninger, 0, 4, 3, 1);
		this.btnVisUdlejninger.setPrefSize(230, 50);
		this.btnVisUdlejninger.setOnAction(event -> visAlleUdlejningerAction());

		this.lblNavnKunde = new Label("Navn");
		this.add(this.lblNavnKunde, 0, 6);
		this.txfNavn = new TextField();
		this.add(this.txfNavn, 1, 6);
		this.txfNavn.setPrefWidth(50);
		this.txfNavn.setPromptText("Navn");

		this.lblAdresse = new Label("Adresse");
		this.add(this.lblAdresse, 0, 7);
		this.txfAdresse = new TextField();
		this.add(this.txfAdresse, 1, 7);
		this.txfAdresse.setPromptText("Adresse");

		this.lblTelefonnummer = new Label("Telefon");
		this.add(this.lblTelefonnummer, 0, 8);
		this.txfTelefonnummer = new TextField();
		this.add(this.txfTelefonnummer, 1, 8);
		this.txfTelefonnummer.setPromptText("Telefonnr");
		this.txfTelefonnummer.setPrefWidth(100);
		this.txfTelefonnummer.setTextFormatter(
				new TextFormatter<>(
						change -> (change.getControlNewText().matches("([1-9][0-9]*)?")) ? change : null));

		this.lblEmail = new Label("Email");
		this.add(this.lblEmail, 0, 9);
		this.txfEmail = new TextField();
		this.add(this.txfEmail, 1, 9);
		this.txfEmail.setPromptText("Email");

		this.lblStartUdlejning = new Label("Start Udlejning");
		this.add(this.lblStartUdlejning, 0, 10);
		this.startDato = new DatePicker();
		this.add(this.startDato, 1, 10);
		this.startDato.setPromptText("Start Dato");

		this.lblSlutUdlejning = new Label("Slut udlejning");
		this.add(this.lblSlutUdlejning, 0, 11);
		this.slutDato = new DatePicker();
		this.add(this.slutDato, 1, 11);
		this.slutDato.setPromptText("Slut Dato");

		this.btnOpretKunde = new Button("Opret kunde");
		this.add(this.btnOpretKunde, 0, 12, 3, 1);
		this.btnOpretKunde.setPrefSize(300, 100);
		this.btnOpretKunde.setOnAction(event -> opretKundeAction());

		udlejningFields(false);
	}

	// Metoder ------------------------------------------------------------------------------------------------

	// Opretter en kunde
	private void opretKundeAction() {
		String navn = this.txfNavn.getText().trim();
		String adresse = this.txfAdresse.getText().trim();
		String telefonNr = this.txfTelefonnummer.getText().trim();
		String email = this.txfEmail.getText().trim();
		LocalDate startDato = this.startDato.getValue();
		LocalDate slutDato = this.slutDato.getValue();

		if (checkFelter() == true) {
			this.kunde = Controller.createKunde(navn, adresse, telefonNr, email);
			this.udlejning = Controller.createUdlejning(startDato, slutDato, this.kunde);
			udlejningFields(true);
			this.lvwProduktKategori.getSelectionModel().select(0);
			this.lvwProdukt.getSelectionModel().select(0);
			this.btnUdlej.setDisable(true);
		}
	}

	// Tilføje udlejningsprodukter til kurven
	private void tilføjTilKurvAction() {
		Produkt produkt = this.lvwProdukt.getSelectionModel().getSelectedItem();

		if (produkt == null) {
			return;
		}
		int antal = Integer.parseInt(this.txfAntal.getText());
		this.ordrelinje = Controller.createOrdreLinje(this.udlejning, produkt, produkt.getNormalpris(), antal);
		this.udlejning.plusPant(this.ordrelinje, true);
		opdaterUdlejningPris();
		this.lvwKurv.getItems().setAll(Controller.getOrdrelinjer(this.kunde));
		nulstilFelter();
		this.btnUdlej.setDisable(false);
	}

	private void selectedProduktKategoriChanged() {
		ProduktKategori kategori = this.lvwProduktKategori.getSelectionModel().getSelectedItem();
		this.lvwProdukt.getItems().setAll(kategori.getProdukter());
	}

	private void fjernFraKurvAction() {
		Ordrelinje ordrelinje = this.lvwKurv.getSelectionModel().getSelectedItem();
		if (ordrelinje == null) {
			this.lblFejl.setText("Din kurv er tom. Du har ingen produkter i kurven.");
			return;
		} else {
			Controller.removeOrdreLinjeFromOrdre(this.udlejning, ordrelinje);
			this.lvwKurv.getItems().setAll(this.udlejning.getOrdrelinjer());
			this.udlejning.plusPant(ordrelinje, false);
			opdaterUdlejningPris();
			nulstilFelter();
		}
	}

	private void selectedProdukterChanged() {
		Produkt produkt = this.lvwProdukt.getSelectionModel().getSelectedItem();
		if (produkt == null) {
			return;
		}
		this.txfProduktNavn.setText(produkt.getNavn());
	}

	// Addere et udlejningsprodukt til ordren
	public void tømKurvAction() {
		Ordrelinje produkt = this.lvwKurv.getSelectionModel().getSelectedItem();
		if (produkt == null) {
			this.lblFejl.setText("Kan ikke tømmes, Da intet produkt valgt");
			return;
		}

		Controller.removeOrdrelinjerFromUdlejning(this.udlejning);

		this.lvwKurv.getItems().clear();
		opdaterUdlejningPris();
		this.txaSamletPris.setText("");
		nulstilFelter();

	}

	// Opdatere den samlede udlejningspris af produkter som er tilføjet til ordren
	public void opdaterUdlejningPris() {
		this.udlejning.setOrdreTotalPris(this.udlejning);
		this.txaSamletPris
				.setText("Antal produkter: " + Controller.getOrdrelinjer(this.kunde).size() + "\nProdukt priser: "
						+ Controller.getOrdreTotalPris(this.udlejning) + "\nPant: " + this.udlejning.getPant());
	}

	// Email validering metode
	public boolean emailIsValid(String s) {
		String str = s;
		String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
				+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(str);
		if (!s.isEmpty() && matcher.matches()) {
			return true;
		}
		return false;
	}

	// Checke om felter er udfyldt korrekt
	public boolean checkFelter() {
		String navn = this.txfNavn.getText().trim();
		String adresse = this.txfAdresse.getText().trim();
		String telefonNr = this.txfTelefonnummer.getText().trim();

		if (navn.matches("[a-zA-Z]*")) {
			this.txfNavn.setStyle("-fx-background-color: lightgreen");
		} else {
			this.txfNavn.setStyle("-fx-background-color: pink");
			this.txfNavn.setText("Må kun indeholde bogstaver");
			return false;
		}
		if (adresse.matches("[a-zA-Z]*")) {
			this.txfAdresse.setStyle("-fx-background-color: lightgreen");
		} else {
			this.txfAdresse.setStyle("-fx-background-color: pink");
			this.txfAdresse.setText("Må kun indeholde bogstaver");
			return false;
		}
		if (telefonNr.length() < 9) {
			this.txfTelefonnummer.setStyle("-fx-background-color: lightgreen");
		} else {
			this.txfTelefonnummer.setStyle("-fx-background-color: pink");
			this.txfTelefonnummer.setText("TelefonNr skal være indtastet");
			return false;
		}

		if (this.startDato.getValue() != null) {
			this.startDato.setStyle("-fx-background-color: lightgreen");
		} else {
			this.startDato.setStyle("-fx-background-color: pink");
			this.startDato.setPromptText("Start dato skal vælges");
			return false;
		}
		if (this.slutDato.getValue() != null) {
			this.slutDato.setStyle("-fx-background-color: lightgreen");
		} else {
			this.slutDato.setStyle("-fx-background-color: pink");
			this.slutDato.setPromptText("Slut dato skal vælges");
			return false;
		}

		// Email validator
		String s = this.txfEmail.getText();
		if (emailIsValid(s)) {
			this.txfEmail.setStyle("-fx-background-color: lightgreen");
		} else {
			this.txfEmail.setStyle("-fx-background-color: pink");
			this.txfEmail.setText("Email format er forkert");
			return false;
		}
		return true;
	}

	public void nulstilFelter() {
		this.lvwKurv.getSelectionModel().select(0);
	}

	private void udlejningFields(boolean open) {
		if (!open) {
			this.lvwProduktKategori.setDisable(true);
			this.lvwProdukt.setDisable(true);
			this.txfProduktNavn.setDisable(true);
			this.txfAntal.setDisable(true);
			this.btnTømKurv.setDisable(true);
			this.btnTilføj.setDisable(true);
			this.btnFjern.setDisable(true);
			this.lvwKurv.setDisable(true);
			this.txaSamletPris.setDisable(true);
			this.btnUdlej.setDisable(true);
		} else {
			this.lvwProduktKategori.setDisable(false);
			this.lvwProdukt.setDisable(false);
			this.txfProduktNavn.setDisable(false);
			this.txfAntal.setDisable(false);
			this.btnTømKurv.setDisable(false);
			this.btnTilføj.setDisable(false);
			this.btnFjern.setDisable(false);
			this.lvwKurv.setDisable(false);
			this.txaSamletPris.setDisable(false);
			this.btnUdlej.setDisable(false);
		}
	}

	public void opretUdlejningAction() {
		if (this.kunde == null) {
			return;
		}
		UdlejningOversigt uo = new UdlejningOversigt("Udlejning - Kunde information & Oversigt", this.udlejning,
				this.kunde);
		uo.showAndWait();
	}

	private void visAlleUdlejningerAction() {
		UdlejningOversigt uo = new UdlejningOversigt("Oversigt over kunde & udlejede produkter",
				Controller.getKunder().get(0).getUdlejninger().get(0),
				Controller.getKunder().get(0));
		uo.showAndWait();
	}
}
