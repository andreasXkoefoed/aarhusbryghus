/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package guiFx;

import java.util.Optional;

import application.controller.Controller;
import application.model.Enhed;
import application.model.Produkt;
import application.model.ProduktKategori;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class LagerVindue extends Stage {
	public LagerVindue(String navn) {
		this.setTitle("Lageret - Aarhus Bryghus");
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);

		this.setResizable(false);

		this.setTitle(navn);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	// ----------------------------------------------------------------------
	// ProduktKategori ---------------------------------------------------
	private TextField txfProduktKategoriNavn = new TextField();
	private final ComboBox<Enhed> cbbEnhed = new ComboBox<>();
	private ListView<ProduktKategori> lvwProduktKategori = new ListView<>();
	private Button btnOpretProduktKategori = new Button("Opret Kategori");
	private final TextArea txaProduktKategoriInfo = new TextArea();
	private CheckBox cbxKlippekort = new CheckBox();

	// Produkt ---------------------------------------------------
	private final Label lblProduktNavn = new Label("Produkt Navn");
	private final Label lblProduktPris = new Label("Pris");
	private final Label lblBeskrivelse = new Label("Beskrivelse");
	private final Label lblProdukter = new Label("Produkter");
	private final Label lblKlippekort = new Label("Klippekort");
	private final Label lblFejl = new Label();
	private final TextField txfProduktNavn = new TextField();
	private final TextField txfProduktPris = new TextField();
	private final TextArea txaProduktInfo = new TextArea();
	private final Button btnBeskrivelse = new Button("Beskrivelse");
	private final Button btnOpretProdukt = new Button("Opret Produkt");
	private final Button btnOpdaterProdukt = new Button("Opdater Produkt");
	private final Button btnSletProdukt = new Button("Slet Produkt");
	private ListView<Produkt> lvwProdukter = new ListView<>();

	String beskrivelse = ""; // Produktbeskrivelsen

	private void initContent(GridPane pane) {

		GridPane kategoriPane = new GridPane();
		pane.add(kategoriPane, 0, 0);
		kategoriPane.setPadding(new Insets(20));
		kategoriPane.setHgap(20);
		kategoriPane.setVgap(10);
		kategoriPane.setGridLinesVisible(false);

		Label lblProduktKategoriHeader = new Label("Produkt Kategori");
		kategoriPane.add(lblProduktKategoriHeader, 0, 0);
		lblProduktKategoriHeader.setStyle("-fx-font-size: 15pt");

		Label lblProduktKategoriNavn = new Label("Navn");
		kategoriPane.add(lblProduktKategoriNavn, 0, 3);
		kategoriPane.add(this.txfProduktKategoriNavn, 0, 4, 2, 1);

		kategoriPane.add(this.lblKlippekort, 2, 3);
		kategoriPane.add(this.cbxKlippekort, 2, 4);

		Label lblEnhed = new Label("Enhed");
		kategoriPane.add(lblEnhed, 3, 3);
		kategoriPane.add(this.cbbEnhed, 3, 4);
		this.cbbEnhed.getItems().addAll(Enhed.values());
		this.cbbEnhed.setValue(Enhed.CL);

		kategoriPane.add(this.btnOpretProduktKategori, 0, 6);
		this.btnOpretProduktKategori.setOnAction(event -> opretProduktKategoriAction());
		this.btnOpretProduktKategori.disableProperty()
				.bind(Bindings.isEmpty(this.txfProduktKategoriNavn.textProperty()));

		Button btnOpdaterProduktKategori = new Button("Opdater kategori");
		this.lvwProduktKategori.getSelectionModel().selectedItemProperty()
				.addListener((p, ov, nv) -> this.selectedProduktKategoriChanged());
		btnOpdaterProduktKategori.setOnAction(event -> opdaterProduktKategoriAction());

		Button btnSletProduktKategori = new Button("Slet kategori");
		kategoriPane.add(btnSletProduktKategori, 3, 6);
		btnSletProduktKategori.setOnAction(event -> sletProduktKategoriAction());

		Label lblProduktKategori = new Label("Produkt Kategorier");
		kategoriPane.add(lblProduktKategori, 0, 8);
		kategoriPane.add(this.lvwProduktKategori, 0, 10, 4, 1);
		this.lvwProduktKategori.getItems().setAll(Controller.getProduktKategorier());
		this.lvwProduktKategori.getSelectionModel().selectedItemProperty()
				.addListener((p, ov, nv) -> this.selectedProduktKategoriChanged());

		Label lblProduktKategoriInfo = new Label("Produkt Kategori info");
		kategoriPane.add(lblProduktKategoriInfo, 0, 11);
		kategoriPane.add(this.txaProduktKategoriInfo, 0, 12, 4, 1);
		this.txaProduktKategoriInfo.setPrefHeight(100);
		this.txaProduktKategoriInfo.setPrefWidth(40);

		// Produkt Pane ----------------------------------------------------------------------------------------------------------

		GridPane produktPane = new GridPane();
		pane.add(produktPane, 1, 0);
		produktPane.setPadding(new Insets(20));
		produktPane.setHgap(20);
		produktPane.setVgap(10);
		produktPane.setGridLinesVisible(false);

		Label lblProduktHeader = new Label("Produkt");
		produktPane.add(lblProduktHeader, 0, 0);
		lblProduktHeader.setStyle("-fx-font-size: 15pt");

		produktPane.add(this.lblFejl, 1, 8, 3, 1);
		this.lblFejl.setTextFill(Color.RED);

		produktPane.add(this.lblProduktNavn, 0, 3);
		produktPane.add(this.txfProduktNavn, 0, 4);

		produktPane.add(this.lblProduktPris, 1, 3);
		produktPane.add(this.txfProduktPris, 1, 4);
		this.txfProduktPris.setMaxWidth(100);
		this.txfProduktPris.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,7}([\\.]\\d{0,4})?")) {
					LagerVindue.this.txfProduktPris.setText(oldValue);
				}
			}
		});

		produktPane.add(this.lblBeskrivelse, 2, 3);
		produktPane.add(this.btnBeskrivelse, 2, 4);
		this.btnBeskrivelse.setOnAction(event -> beskrivelseAction());
		this.btnBeskrivelse.disableProperty()
				.bind(Bindings.isEmpty(this.txfProduktNavn.textProperty()));

		produktPane.add(this.btnOpretProdukt, 0, 6);
		this.btnOpretProdukt.setOnAction(event -> opretProduktAction());
		this.btnOpretProdukt.disableProperty()
				.bind(Bindings.isEmpty(this.txfProduktNavn.textProperty()));

		produktPane.add(this.btnOpdaterProdukt, 1, 6);
		this.btnOpdaterProdukt.setOnAction(event -> opdaterProduktAction());
		produktPane.add(this.btnSletProdukt, 2, 6);
		this.btnSletProdukt.setOnAction(event -> sletProduktAction());

		produktPane.add(this.lblProdukter, 0, 8);
		produktPane.add(this.lvwProdukter, 0, 10, 3, 1);
		this.lvwProdukter.getItems().addAll(Controller.getProdukter());
		this.lvwProdukter.getSelectionModel().selectedItemProperty()
				.addListener((p, ov, nv) -> this.selectedProdukterChanged());
		this.lvwProdukter.setPrefWidth(400);

		Label lblProduktInfo = new Label("Produkt Info");
		produktPane.add(lblProduktInfo, 0, 11);
		produktPane.add(this.txaProduktInfo, 0, 12, 3, 1);
		this.txaProduktInfo.setPrefHeight(100);
		this.txaProduktInfo.setPrefWidth(400);
	}
	// ProduktKategori metoder ------------------------------

	private void selectedProduktKategoriChanged() {
		ProduktKategori produktkategori = this.lvwProduktKategori.getSelectionModel().getSelectedItem();
		if (produktkategori == null) {
			return;
		}

		this.txaProduktKategoriInfo
				.setText("Kategori: " + produktkategori.getNavn() + "\nEnhed: " + produktkategori.getEnhed()
						+ "\nTilgængelig til udlejning: " + produktkategori.isUdlejning() + " kr,-"
						+ "\nAntal produkter i kategorien: "
						+ produktkategori.getProdukter().size() + "\nKlipværdi: "
						+ produktkategori.getKlipværdi());
		this.txfProduktKategoriNavn.setText(produktkategori.getNavn());
		this.cbbEnhed.setValue(produktkategori.getEnhed());
		this.lvwProdukter.getItems().setAll(produktkategori.getProdukter());
		clearFields();
	}

	private void sletProduktKategoriAction() {
		ProduktKategori produktkategori = this.lvwProduktKategori.getSelectionModel().getSelectedItem();
		if (produktkategori != null) {
			Stage s = (Stage) this.getScene().getWindow();
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Slet Produkt Kategori");
			alert.initOwner(s);
			alert.setHeaderText(
					"Er du sikker på at du vil slette kategorien " + produktkategori.getNavn()
							+ "?" + "\n\nOBS. Handlingen kan ikke fortrydes.");
			Optional<ButtonType> result = alert.showAndWait();
			if (result.isPresent() && result.get() == ButtonType.OK) {
				Controller.deleteProduktKategori(produktkategori);
			}
			this.lvwProduktKategori.getItems().clear();
			this.lvwProduktKategori.getItems().setAll(Controller.getProduktKategorier());
		}
	}

	private void opdaterProduktKategoriAction() {
		ProduktKategori produktkategori = this.lvwProduktKategori.getSelectionModel().getSelectedItem();
		String navn = this.txfProduktKategoriNavn.getText().trim();
		Enhed enhed = this.cbbEnhed.getValue();
		if (produktkategori == null || navn.isEmpty()) {
			return;
		} else {
			Controller.updateProduktKategori(produktkategori, navn, enhed);
			this.clearFields();
			this.lvwProduktKategori.getItems().setAll(Controller.getProduktKategorier());
		}
	}

	private void opdaterProduktAction() {
		Produkt produkt = this.lvwProdukter.getSelectionModel().getSelectedItem();
		String beskrivelse;
		if (produkt == null || this.txfProduktNavn.getText().isEmpty() == true
				|| this.txfProduktPris.getText().isEmpty() == true) {
			return;
		}
		if (!this.beskrivelse.isEmpty()) {
			beskrivelse = this.beskrivelse;
		} else {
			beskrivelse = produkt.getBeskrivelse();
		}
		String navn = this.txfProduktNavn.getText().trim();
		int pris = Integer.parseInt(this.txfProduktPris.getText());
		Controller.updateProdukt(produkt, navn, pris, beskrivelse);
		this.lvwProdukter.getItems().setAll(produkt.getProduktkategori().getProdukter());
	}

	private void opretProduktKategoriAction() {
		String navn = this.txfProduktKategoriNavn.getText().trim();
		navn = navn.replaceAll("\\s", "");
		Enhed enhed = this.cbbEnhed.getValue();
		if (navn == null || navn.trim().equals("")) {
			return;
		}
		Controller.createProduktKategori(navn, enhed, false, 0);
		this.clearFields();
		this.txfProduktKategoriNavn.requestFocus();
		this.lvwProduktKategori.getItems().setAll(Controller.getProduktKategorier());

	}

	private void clearFields() {
		this.txfProduktKategoriNavn.setText("");
		this.cbbEnhed.setValue(Enhed.CL);
		this.txfProduktNavn.setText("");
		this.txfProduktPris.setText("");
		this.beskrivelse = "";
	}

	// Produkt metoder ------------------------------
	private void selectedProdukterChanged() {
		Produkt produkt = this.lvwProdukter.getSelectionModel().getSelectedItem();
		if (produkt == null) {
			return;
		}
		this.txfProduktNavn.setText(produkt.getNavn());
		this.txfProduktPris.setText(Integer.toString((int) produkt.getNormalpris()));
		this.txaProduktInfo.setText("Produkt: " + produkt.getNavn() + "\nPris: " + produkt.getNormalpris() + ", Enhed: "
				+ produkt.getProduktkategori().getEnhed() + "\n\nBeskrivelse: \n + " + produkt.getBeskrivelse());
	}

	private void beskrivelseAction() {

		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Produkt beskrivelse");
		dialog.setHeaderText("Giv produktet en beskrivelse");
		dialog.setContentText("Beskrivelse:");

		Optional<String> result = dialog.showAndWait();
		//wait for the dialog to close

		if (result.isPresent()) {
			String response = result.get();
			this.beskrivelse = response;
		}
	}

	private void opretProduktAction() {
		ProduktKategori kategori = this.lvwProduktKategori.getSelectionModel().getSelectedItem();
		String navn = this.txfProduktNavn.getText().trim();
		navn = navn.replaceAll("\\s", "");

		if (this.txfProduktPris.getText().isEmpty()) {
			this.lblFejl.setText("OBS! Produktnavn & Produktpris skal være indtastet");
		}

		if (kategori == null || navn == null || navn.trim().equals("")
				|| this.txfProduktNavn.getText().isEmpty()
				|| this.txfProduktPris.getText().isEmpty()) {
			return;
		} else {
			int pris = Integer.parseInt(this.txfProduktPris.getText().trim());
			Controller.createProdukt(kategori, navn, pris, this.beskrivelse);
			this.clearFields();
			this.lvwProdukter.getItems().setAll(kategori.getProdukter());
			this.txfProduktNavn.requestFocus();
		}
	}

	private void sletProduktAction() {
		ProduktKategori kategori = this.lvwProduktKategori.getSelectionModel().getSelectedItem();
		Produkt produkt = this.lvwProdukter.getSelectionModel().getSelectedItem();
		if (kategori != null && produkt != null) {
			Stage s = (Stage) this.getScene().getWindow();
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Slet Produkt Kategori");
			alert.initOwner(s);
			alert.setHeaderText(
					"Er du sikker på at du vil slette produktet " + produkt.getNavn() + "?"
							+ "\n\nOBS. Handlingen kan ikke fortrydes.");
			Optional<ButtonType> result = alert.showAndWait();
			if (result.isPresent() && result.get() == ButtonType.OK) {
				Controller.deleteProduktKategori(kategori);
			}
			Controller.deleteProdukt(kategori, produkt);
			this.lvwProdukter.getItems().setAll(kategori.getProdukter());
		}
	}

}
