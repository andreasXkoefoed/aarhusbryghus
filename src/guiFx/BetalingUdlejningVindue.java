/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package guiFx;

import java.util.Optional;

import application.controller.Controller;
import application.model.BetalingsType;
import application.model.Kunde;
import application.model.Udlejning;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class BetalingUdlejningVindue extends Stage {
	private Udlejning udlejning;
	private Kunde kunde;
	boolean pantBetaling;

	public BetalingUdlejningVindue(String title, Udlejning udlejning, Kunde kunde, Boolean pantBetaling) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.udlejning = udlejning;
		this.kunde = kunde;
		this.pantBetaling = pantBetaling;

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	private Label lblSamletPris = new Label();
	private Button btnDankort = new Button("Dankort");
	private Button btnKontant = new Button("Kontant");
	private Button btnMobilePay = new Button("MobilePay");
	private Button btnKlippekort = new Button("Klippekort");
	private Button btnRegning = new Button("Regning");

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		pane.add(this.lblSamletPris, 0, 0, 4, 1);
		this.lblSamletPris.setPrefHeight(150);
		this.lblSamletPris.setFont(new Font(15));

		pane.add(this.btnDankort, 0, 1);
		this.btnDankort.setPrefSize(100, 75);
		this.btnDankort.setOnAction(event -> dankortAction());

		pane.add(this.btnMobilePay, 1, 1);
		this.btnMobilePay.setPrefSize(100, 75);
		this.btnMobilePay.setOnAction(event -> mobilePayAction());

		pane.add(this.btnKontant, 2, 1);
		this.btnKontant.setPrefSize(100, 75);
		this.btnKontant.setOnAction(event -> kontantAction());

		pane.add(this.btnKlippekort, 3, 1);
		this.btnKlippekort.setPrefSize(100, 75);
		this.btnKlippekort.setOnAction(event -> klippekortAction());

		pane.add(this.btnRegning, 4, 1);
		this.btnRegning.setPrefSize(100, 75);
		this.btnRegning.setOnAction(event -> regningAction());

		if (this.pantBetaling) {
			betalPant();
		} else {
			samletBetaling();
		}
	}

	// Hvis panten skal betales
	public void betalPant() {
		this.lblSamletPris.setText("Navn: " + this.kunde.getNavn()
				+ "\nKundeID: " + this.kunde.getKundeID()
				+ "\nUdlejning ID: " + this.udlejning.getUdlejningID()
				+ "\nAntal udlejede produkter: " + Controller.getOrdrelinjer(this.kunde).size()
				+ "\nPant pris: " + this.udlejning.getPant() + " kr,-"
				+ "\n\nPris som skal betales nu: " + this.udlejning.getPant() + " kr,-");
		this.btnKlippekort.setDisable(true);
		this.btnRegning.setDisable(true);
	}

	// Hvis den samlede ordre skal betales + pant afregning
	public void samletBetaling() {
		this.lblSamletPris.setText(
				"Pris for samlet udlejning: " + this.udlejning.getSamletPris() + " kr, -"
						+ "\nPris for ikke forbrugte varer " + this.udlejning.ikkeForbrugteProdukterPris()
						+ "\nPant pris: "
						+ this.udlejning.getPant() + "kr, -"
						+ "\n\nTotal pris for ordren: "
						+ this.udlejning.totalPris() + " kr, - ");
	}

	public void getChoiceAction(String overskrift, String pantStatus, String ordreStatus) {
		Stage s = (Stage) this.getScene().getWindow();
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(overskrift);
		alert.initOwner(s);
		alert.setHeaderText("Betaling for: " + overskrift + "\n\nFor kunden: " + this.kunde.getNavn()
				+ "\nPanten er: " + pantStatus + "\nBeløb: "
				+ this.udlejning.getPant() + "\nBetalingsmetode: "
				+ this.udlejning.getBetalingstype() + "\n\nSamlet pris for ordre: "
				+ this.udlejning.totalPris()
				+ "\nOrdre er: " + ordreStatus);
		Optional<ButtonType> result = alert.showAndWait();
		if (result.isPresent() && result.get() == ButtonType.OK) {

		}
	}

	private void regningAction() {
		Controller.setBetalingsMetode(this.udlejning, BetalingsType.REGNING);
		this.udlejning.setBetalt(true);
		hide();
	}

	private void klippekortAction() {
		Controller.setBetalingsMetode(this.udlejning, BetalingsType.KLIPPEKORT);
		this.udlejning.setBetalt(true);
		hide();
	}

	private void kontantAction() {
		Controller.setBetalingsMetode(this.udlejning, BetalingsType.KONTANT);
		if (this.pantBetaling) {
			this.udlejning.setPantBetalt(true);
			getChoiceAction("PANT", "BETALT", "IKKE BETALT");
		} else {
			this.udlejning.setBetalt(true);
			getChoiceAction("ORDRE", "BETALT", "BETALT");
		}
		hide();
	}

	private void mobilePayAction() {
		Controller.setBetalingsMetode(this.udlejning, BetalingsType.MOBILEPAY);
		if (this.pantBetaling) {
			this.udlejning.setPantBetalt(true);
			getChoiceAction("PANT", "BETALT", "IKKE BETALT");
		} else {
			this.udlejning.setBetalt(true);
			getChoiceAction("ORDRE", "BETALT", "BETALT");
		}
		hide();
	}

	private void dankortAction() {
		Controller.setBetalingsMetode(this.udlejning, BetalingsType.DANKORT);
		if (this.pantBetaling) {
			this.udlejning.setPantBetalt(true);
			getChoiceAction("PANT", "BETALT", "IKKE BETALT");
		} else {
			this.udlejning.setBetalt(true);
			getChoiceAction("ORDRE", "BETALT", "BETALT");
		}
		hide();
	}
}
