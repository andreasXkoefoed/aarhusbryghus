/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package guiFx;

import application.controller.Controller;
import application.model.Ordre;
import application.model.Ordrelinje;
import application.model.Prisliste;
import application.model.ProduktKategori;
import application.model.Salgssituation;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class Butik extends GridPane {

	// --------------------------------------------------------------------
	private ComboBox<Salgssituation> cbbSalgssituation = new ComboBox<>();
	private ListView<Prisliste> lvwProdukt = new ListView<>();
	private ListView<ProduktKategori> lvwProduktKategori = new ListView<ProduktKategori>();
	private Button btnTømKurv = new Button("Tøm kurv");
	private Button btnTilføj = new Button("Tilføj til kurv");
	private Button btnFjern = new Button("Fjern fra kurv");
	private Button btnBetaling = new Button("Betaling");
	private ListView<Ordrelinje> lvwOrdrelinje = new ListView<>();
	private TextArea txaSamletPris = new TextArea();
	private TextField txfProduktNavn = new TextField();
	private TextField txfAntal = new TextField();
	private TextField txfRabat = new TextField();
	private Label lblFejl = new Label();

	public Butik() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		this.setMaxHeight(1000);

		Label lblSalgText = new Label("Salg for Aarhus Bryghus");
		this.add(lblSalgText, 0, 0);
		lblSalgText.setStyle("-fx-font-size: 15pt");

		Label lblSalgssituation = new Label("Salgssituation");
		this.add(lblSalgssituation, 0, 3);
		this.add(this.cbbSalgssituation, 0, 4);
		this.cbbSalgssituation.setMaxHeight(100);
		this.cbbSalgssituation.getItems().setAll(Controller.getSalgssituationer());
		this.cbbSalgssituation.getSelectionModel().select(Controller.getSalgssituationer().get(1));

		Label lblProduktKategori = new Label("ProduktKategori");
		this.add(lblProduktKategori, 0, 6);
		this.add(this.lvwProduktKategori, 0, 8, 1, 5);
		this.lvwProduktKategori.getItems()
				.setAll(Controller.getProduktsKategori(Controller.getSalgssituationer().get(1)));
		this.lvwProduktKategori.getSelectionModel().select(0);
		this.cbbSalgssituation.getSelectionModel().selectedItemProperty()
				.addListener((p, ov, nv) -> this.selectedSalgssituationChanged());
		Label lblProdukter = new Label("Produkter");
		this.add(lblProdukter, 1, 6);
		this.add(this.lvwProdukt, 1, 8, 1, 5);
		this.lvwProdukt.setPrefWidth(250);
		this.lvwProdukt.getItems()
				.setAll(Controller.getSalgssituationer().get(1).getPrislister());
		this.lvwProduktKategori.getSelectionModel().selectedItemProperty()
				.addListener((p, ov, nv) -> this.selectedProduktKategoriChanged());

		this.lvwProdukt.getSelectionModel().selectedItemProperty()
				.addListener((p, ov, nv) -> this.selectedProdukterChanged());

		Label lblValgtProdukt = new Label("Det valgte produkt");
		this.add(lblValgtProdukt, 2, 6);

		this.add(this.txfProduktNavn, 2, 8);
		this.txfProduktNavn.setMaxWidth(200);
		this.txfProduktNavn.setEditable(false);
		this.txfProduktNavn.setPromptText("Produkt navn");

		this.txfAntal.setMaxWidth(50);
		this.txfAntal.setEditable(false);
		this.txfAntal.setPromptText("Antal");

		Label lblRabat = new Label("Rabat");
		this.add(lblRabat, 2, 13);
		lblRabat.setAlignment(Pos.BOTTOM_RIGHT);
		this.add(this.txfRabat, 2, 14);
		this.txfRabat.setMaxWidth(200);
		this.txfRabat.setPromptText("Rabat");
		this.txfRabat.setTextFormatter(
				new TextFormatter<>(
						change -> (change.getControlNewText().matches("([1-9][0-9]*)?")) ? change : null));
		this.txfRabat.textProperty().addListener((observable, oldValue, newValue) -> {
			if (newValue.length() >= 3) {
				this.txfRabat.setText(oldValue);
			}
			if (this.txfRabat.getText().length() == 0) {
				opdaterTotalPrisAction();
			}
			if (this.lvwOrdrelinje.getItems().size() > 0) {
				opdaterTotalPrisAction();
			}
		});

		this.add(this.btnTømKurv, 3, 8);
		this.btnTømKurv.setPrefSize(200, 50);
		this.btnTømKurv.setOnAction(plus -> tømKurvAction());

		this.add(this.btnTilføj, 2, 9);
		this.btnTilføj.setPrefSize(200, 50);
		this.btnTilføj.setOnAction(event -> tilføjTilKurvAction());

		this.add(this.btnFjern, 3, 9, 3, 1);
		this.btnFjern.setPrefSize(200, 50);
		this.btnFjern.setOnAction(event -> fjernFraKurvAction());

		this.add(this.lblFejl, 2, 0, 3, 1);
		this.lblFejl.setTextFill(Color.RED);

		Label lblKurv = new Label("Kurven");
		this.add(lblKurv, 2, 11);
		this.add(this.lvwOrdrelinje, 2, 12, 4, 1);
		this.lvwOrdrelinje.setMaxWidth(360);

		this.add(this.btnBetaling, 3, 15);
		this.btnBetaling.setPrefSize(100, 60);
		this.btnBetaling.setOnAction(event -> betalingAction());

		Label lblSamletPris = new Label("Samlet Pris");
		this.add(lblSamletPris, 0, 13);
		this.add(this.txaSamletPris, 0, 14, 2, 2);
		this.txaSamletPris.setText("0.0");
		this.txaSamletPris.setPrefWidth(100);
		this.txaSamletPris.setPrefHeight(100);
		this.txaSamletPris.setWrapText(true);
		this.txaSamletPris.setEditable(false);
		this.txaSamletPris.setFont(new Font(15));
	}

	private void selectedProdukterChanged() {
		Prisliste produktpris = this.lvwProdukt.getSelectionModel().getSelectedItem();
		this.txaSamletPris
				.setText("" + Controller.udregnPrisMedRabat(this.ordre));
		if (produktpris == null) {
			return;
		}
		this.txfProduktNavn.setText(produktpris.getProdukt().getNavn());
		this.txfAntal.setText("1");
	}

	private void selectedProduktKategoriChanged() {
		Salgssituation s = this.cbbSalgssituation.getSelectionModel().getSelectedItem();
		ProduktKategori kategori = this.lvwProduktKategori.getSelectionModel().getSelectedItem();
		this.lvwProdukt.getItems().clear();
		this.txaSamletPris
				.setText("" + Controller.udregnPrisMedRabat(this.ordre));
		for (Prisliste pris : s.getPrislister()) {
			if (pris.getProdukt().getProduktkategori().equals(kategori)) {
				this.lvwProdukt.getItems().addAll(pris);
			}
		}
	}

	public void selectedSalgssituationChanged() {
		this.lvwProdukt.getItems().clear();
		Salgssituation s = this.cbbSalgssituation.getSelectionModel().getSelectedItem();
		this.lvwProdukt.getItems().addAll(Controller.getProduktprisOnSalgssituation(s));
		this.ordre = new Ordre();
		this.lvwOrdrelinje.getItems().clear();
		this.lvwProdukt.getItems().addAll(Controller.getProduktprisOnSalgssituation(s));
		this.lvwProduktKategori.getItems().setAll(Controller.getProduktsKategori(s));
		this.lvwProduktKategori.getSelectionModel().select(0);
		this.btnBetaling.setText(s.getNavn() + "\nBetaling");
	}

	// -------------------------------------------------

	// Knapper SetOnAction
	Ordre ordre = Controller.createOrdre();

	public void tømKurvAction() {

		Prisliste produkt = this.lvwProdukt.getSelectionModel().getSelectedItem();
		if (produkt == null) {
			this.lblFejl.setText("Kan ikke tømmes, Da intet produkt valgt");
			return;
		}

		this.ordre = Controller.createOrdre();
		this.lvwOrdrelinje.getItems().clear();
		opdaterTotalPrisAction();
		this.txaSamletPris.setText("0");
	}

	int pant = 0;

	private void tilføjTilKurvAction() {
		this.lblFejl.setText("");
		Prisliste produkt = this.lvwProdukt.getSelectionModel().getSelectedItem();
		if (produkt == null) {
			this.lblFejl.setText("Produkt skal først vælges");
			return;
		} else {
			int antal = Integer.parseInt(this.txfAntal.getText());
			Controller.createOrdreLinje(this.ordre, produkt.getProdukt(), produkt.getPris(), antal);
			if (this.pant >= 0) {
				this.pant += produkt.getProdukt().getProduktkategori().getPantPris();
			}
			opdaterTotalPrisAction();
			this.lvwOrdrelinje.getItems().setAll(this.ordre.getOrdrelinjer());
			this.lvwOrdrelinje.getSelectionModel().select(0);
		}
	}

	public double getRabat() {
		double rabat = 0;
		return rabat = Controller.getOrdreTotalPris(this.ordre) * (1 - (rabat / 100));
	}

	private void fjernFraKurvAction() {
		Ordrelinje ordrelinje = this.lvwOrdrelinje.getSelectionModel().getSelectedItem();
		Prisliste produkt = this.lvwProdukt.getSelectionModel().getSelectedItem();
		if (produkt == null) {
			this.lblFejl.setText("Produkt skal først vælges");
			return;
		}
		Controller.removeOrdreLinjeFromOrdre(this.ordre, ordrelinje);
		this.lvwOrdrelinje.getItems().setAll(this.ordre.getOrdrelinjer());
		if (this.pant > 0) {
			this.pant -= produkt.getProdukt().getProduktkategori().getPantPris();
		}
		opdaterTotalPrisAction();
		this.lvwOrdrelinje.getSelectionModel().select(0);
	}

	private void opdaterTotalPrisAction() {
		ProduktKategori k = this.lvwProduktKategori.getSelectionModel().getSelectedItem();
		this.ordre.setOrdreTotalPris(this.ordre);
		double produktpriser = Controller.getOrdreTotalPris(this.ordre);

		if (k.isUdlejning()) {
			if (k.equals(Controller.getProduktKategorier().get(6))
					|| k.equals(Controller.getProduktKategorier().get(7))) {
				this.txaSamletPris
						.setText("Antal produkter: " + this.ordre.getOrdrelinjer().size() + "\nProdukt priser: "
								+ produktpriser + "\nPant: " + this.pant + "\nSamlet pris: "
								+ (produktpriser + this.pant));
			}
		}
		if (this.cbbSalgssituation.getSelectionModel().getSelectedItem()
				.equals(Controller.getSalgssituationer().get(0))) {
			this.txaSamletPris
					.setText("" + Controller.getOrdreTotalPris(this.ordre) + "\n Værdi i Klip:"
							+ Controller.samletAntalKlip(this.ordre));
			if (!this.txfRabat.getText().isEmpty()) {
				double rabat = Integer.parseInt(this.txfRabat.getText().trim());
				double pris = Controller.getOrdreTotalPris(this.ordre) * (1 - (rabat / 100));
				this.txaSamletPris.setText("" + pris);
			}
		} else {
			double pris = Controller.getOrdreTotalPris(this.ordre);
			this.txaSamletPris.setText("" + pris);
		}
		if (!this.txfRabat.getText().isEmpty()) {
			int rabat = Integer.parseInt(this.txfRabat.getText().trim());
			this.ordre.setRabat(rabat);
			Controller.udregnPrisMedRabat(this.ordre);
			this.txaSamletPris.setText("" + this.ordre.getSamletPris());
		}
	}

	private void betalingAction() {
		if (this.lvwOrdrelinje.getItems().isEmpty()) {
			this.lblFejl.setText("Tilføj produkter først. Din ordre er tom.");
			return;
		}
		ProduktKategori k = this.lvwProduktKategori.getSelectionModel().getSelectedItem();
		if (!k.isUdlejning()) {
			this.ordre.setOrdreTotalPris(this.ordre);
			BetalingButikVindue bv = new BetalingButikVindue("Betaling", this.ordre);
			bv.showAndWait();

			if (this.ordre.isBetalt()) {
				opdaterTotalPrisAction();
				this.ordre = Controller.createOrdre();
				this.lvwOrdrelinje.getItems().clear();
				this.txfRabat.setText("");
				this.txaSamletPris.setText("0.0");
			}

		} else {
			this.ordre.setOrdreTotalPris(this.ordre);
		}
	}
}
