/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package guiFx;

import java.time.LocalDate;

import application.controller.Controller;
import application.model.Ordre;
import application.model.Ordrelinje;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

public class Statistik extends GridPane {

	// --------------------------------------------------------------------

	private ListView<Ordre> lvwOrdrer = new ListView<>();
	private ListView<Ordrelinje> lvwKundeProdukter = new ListView<>();

	private final DatePicker dateInOrdre = new DatePicker();
	private final DatePicker dateInSolgteKlip = new DatePicker();
	private final DatePicker dateInKlippekort = new DatePicker();
	private final DatePicker dateOutOrdre = new DatePicker();
	private final DatePicker dateOutSolgteKlip = new DatePicker();
	private final DatePicker dateOutKlippekort = new DatePicker();

	private final TextField txfKlipSolgt = new TextField();
	private final TextField txfKlipBrugt = new TextField();
	private final TextField txfDagensSalg = new TextField();

	private Button btnOrdre = new Button("Vis Statistik");
	private Button btnSolgteKlippekort = new Button("Vis Statistik");
	private Button btnKlippekort = new Button("Vis Statistik");
	private Button btnDagensSalg = new Button("Vis Salg");

	private Label lblFejl = new Label();

	// --------------------------------------------------------------------

	public Statistik() {
		this.setPadding(new Insets(20));
		this.setHgap(20);
		this.setVgap(10);
		this.setGridLinesVisible(false);
		this.setMaxHeight(1000);

		Label lblStatestik = new Label("Statistik For Aarhus Bryghus");
		this.add(lblStatestik, 0, 0);
		lblStatestik.setStyle("-fx-font-size: 15pt");

		this.add(this.dateInOrdre, 0, 1);
		this.dateInOrdre.setPrefWidth(200);
		this.dateInOrdre.setPromptText("Skriv fra dagen");

		this.add(this.lblFejl, 1, 1);
		this.lblFejl.setTextFill(Color.RED);

		this.add(this.dateOutOrdre, 0, 2);
		this.dateOutOrdre.setPrefWidth(200);
		this.dateOutOrdre.setPromptText("Skriv til dagen");

		this.add(this.dateInSolgteKlip, 2, 9);
		this.dateInSolgteKlip.setPrefWidth(200);
		this.dateInSolgteKlip.setPromptText("Skriv fra dagen");

		this.add(this.dateOutSolgteKlip, 2, 10);
		this.dateOutSolgteKlip.setPrefWidth(200);
		this.dateOutSolgteKlip.setPromptText("Skriv til dagen");

		this.add(this.dateInKlippekort, 2, 2);
		this.dateInKlippekort.setPrefWidth(200);
		this.dateInKlippekort.setPromptText("Skriv fra dagen");

		this.add(this.dateOutKlippekort, 2, 3);
		this.dateOutKlippekort.setPrefWidth(200);
		this.dateOutKlippekort.setPromptText("Skriv til dagen");

		Label lblKlippekortDato = new Label("Statistik for Brugte Klippekort");
		this.add(lblKlippekortDato, 2, 1);
		this.add(this.txfKlipBrugt, 2, 7);
		this.btnKlippekort.setOnAction(event -> brugteKlipAction());

		this.add(this.btnOrdre, 0, 3);
		this.add(this.btnSolgteKlippekort, 2, 11);
		this.add(this.btnKlippekort, 2, 4);
		this.add(this.btnDagensSalg, 0, 17);

		Label lblOrdre = new Label("Statistik for Ordre");
		this.add(lblOrdre, 0, 4);
		this.add(this.lvwOrdrer, 0, 5, 1, 10);
		this.btnOrdre.setOnAction(event -> ordreAction());

		Label lblSolgteKlipDato = new Label("Statistik for Solgte Klippekort");
		this.add(lblSolgteKlipDato, 2, 8);
		this.add(this.txfKlipSolgt, 2, 13);
		this.btnSolgteKlippekort.setOnAction(event -> solgteKlipAction());

		this.add(this.lvwKundeProdukter, 1, 5, 1, 10);
		this.lvwOrdrer.getSelectionModel().selectedItemProperty()
				.addListener((p, ov, nv) -> this.selectedOrdreChanged());

		Label lblDagensSalg = new Label("Dagens Salg");
		this.add(lblDagensSalg, 0, 16);
		this.add(this.txfDagensSalg, 0, 18);
		this.btnDagensSalg.setOnAction(event -> DagensSalgAction());
	}

	private void solgteKlipAction() {
		if (this.dateInSolgteKlip.getValue() == null && this.dateOutSolgteKlip.getValue() == null
				|| this.dateInSolgteKlip.getValue() == null || this.dateOutSolgteKlip.getValue() == null) {
			this.lblFejl.setText("Begge dato er ikke valgt for solgte Klippekort");
		} else {
			this.txfKlipSolgt.setText(
					"" + Controller.KlipSoldPeriode(this.dateInSolgteKlip.getValue(),
							this.dateOutSolgteKlip.getValue()));
			this.lblFejl.setText("");
		}
	}

	private void DagensSalgAction() {
		this.txfDagensSalg.setText("" + Controller.getDagensSalg(LocalDate.now()));
	}

	private void ordreAction() {

		if (this.dateInOrdre.getValue() == null && this.dateOutOrdre.getValue() == null
				|| this.dateInOrdre.getValue() == null || this.dateOutOrdre.getValue() == null) {
			this.lblFejl.setText("Begge dato er ikke valgt for Ordre");
		} else {
			this.lvwOrdrer.getItems()
					.setAll(Controller.BetalteOrdreIPeriode(this.dateInOrdre.getValue(), this.dateOutOrdre.getValue()));
			this.lblFejl.setText("");
		}
	}

	public void selectedOrdreChanged() {
		Ordre ordre = this.lvwOrdrer.getSelectionModel().getSelectedItem();
		this.lvwKundeProdukter.getItems().clear();
		this.lvwKundeProdukter.getItems().addAll(ordre.getOrdrelinjer());
	}

	private void brugteKlipAction() {

		if (this.dateInKlippekort.getValue() == null && this.dateOutKlippekort.getValue() == null
				|| this.dateInKlippekort.getValue() == null || this.dateOutKlippekort.getValue() == null) {
			this.lblFejl.setText("Begge dato er ikke valgt for brugte Klippekort");
		} else {
			this.txfKlipBrugt.setText(
					"" + Controller.KlipBrugtPeriode(this.dateInKlippekort.getValue(),
							this.dateOutKlippekort.getValue()));
			this.lblFejl.setText("");
		}
	}
}
