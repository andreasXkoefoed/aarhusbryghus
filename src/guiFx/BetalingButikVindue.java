/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package guiFx;

import application.controller.Controller;
import application.model.BetalingsType;
import application.model.Ordre;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class BetalingButikVindue extends Stage {
	private Ordre ordre;

	public BetalingButikVindue(String title, Ordre ordre) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.ordre = ordre;

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		this.setScene(scene);

	}

	private Label lblSamletPris = new Label();
	private Button btnDankort = new Button("Dankort");
	private Button btnKontant = new Button("Kontant");
	private Button btnMobilePay = new Button("MobilePay");
	private Button btnKlippekort = new Button("Klippekort");
	private Button btnRegning = new Button("Regning");
	private Button btnCancel = new Button("Cancel");

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		pane.add(this.lblSamletPris, 0, 0, 4, 1);
		this.lblSamletPris.setPrefHeight(150);
		this.lblSamletPris.setFont(new Font(40));
		this.lblSamletPris.setText("                  " + Controller.udregnPrisMedRabat(this.ordre) + " kr, -");

		pane.add(this.btnDankort, 0, 1);
		this.btnDankort.setPrefSize(100, 75);
		this.btnDankort.setOnAction(event -> dankortAction());

		pane.add(this.btnMobilePay, 1, 1);
		this.btnMobilePay.setPrefSize(100, 75);
		this.btnMobilePay.setOnAction(event -> mobilePayAction());

		pane.add(this.btnKontant, 2, 1);
		this.btnKontant.setPrefSize(100, 75);
		this.btnKontant.setOnAction(event -> kontantAction());

		pane.add(this.btnKlippekort, 3, 1);
		this.btnKlippekort.setPrefSize(100, 75);
		this.btnKlippekort.setOnAction(event -> klippekortAction());

		pane.add(this.btnRegning, 4, 1);
		this.btnRegning.setPrefSize(100, 75);
		this.btnRegning.setOnAction(event -> regningAction());

		pane.add(this.btnCancel, 0, 2, 5, 2);
		this.btnCancel.setPrefSize(540, 75);
		this.btnCancel.setOnAction(event -> cancelAction());
	}

	private void regningAction() {
		Controller.setBetalingsMetode(this.ordre, BetalingsType.REGNING);
		this.ordre.setBetalingsType(BetalingsType.REGNING);
		this.ordre.setBetalt(true);
		hide();
	}

	private void klippekortAction() {
		Controller.setBetalingsMetode(this.ordre, BetalingsType.KLIPPEKORT);
		this.ordre.setBetalingsType(BetalingsType.KLIPPEKORT);
		this.ordre.setBetalt(true);
		for (Ordre ol : Controller.getOrdre()) {
			ol.setKlipBrugt(this.ordre.getKlipBrugt());
		}
		hide();
	}

	private void kontantAction() {
		Controller.setBetalingsMetode(this.ordre, BetalingsType.KONTANT);
		this.ordre.setBetalt(true);
		this.ordre.setBetalingsType(BetalingsType.KONTANT);
		hide();
	}

	private void mobilePayAction() {
		Controller.setBetalingsMetode(this.ordre, BetalingsType.MOBILEPAY);
		this.ordre.setBetalt(true);
		this.ordre.setBetalingsType(BetalingsType.MOBILEPAY);
		hide();
	}

	private void dankortAction() {
		Controller.setBetalingsMetode(this.ordre, BetalingsType.DANKORT);
		this.ordre.setBetalt(true);
		this.ordre.setBetalingsType(BetalingsType.DANKORT);
		hide();
	}

	private void cancelAction() {
		this.ordre.setBetalt(false);
		hide();
	}
}
