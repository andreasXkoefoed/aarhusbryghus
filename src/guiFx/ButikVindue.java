/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package guiFx;

import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ButikVindue extends Stage {
	public ButikVindue(String navn) {
		this.setTitle("Butikken - Aarhus Bryghus");
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);

		BorderPane borderPane = new BorderPane();

		this.initContent(borderPane);
		Scene scene = new Scene(borderPane);
		this.setScene(scene);
	}

	private void initContent(BorderPane pane) {
		TabPane tabPane = new TabPane();
		this.initTabPane(tabPane);
		pane.setCenter(tabPane);
	}

	private void initTabPane(TabPane tabPane) {
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
		Butik butik = new Butik();

		Tab tabButik = new Tab("Butikken");
		tabPane.getTabs().add(tabButik);
		tabButik.setContent(butik);

		UdlejningTab udlejning = new UdlejningTab();

		Tab tabUdlejning = new Tab("Udlejning");
		tabPane.getTabs().add(tabUdlejning);
		tabUdlejning.setContent(udlejning);

		Statistik statestik = new Statistik();

		Tab tabStatestik = new Tab("Statistik");
		tabPane.getTabs().add(tabStatestik);
		tabStatestik.setContent(statestik);

	}
}