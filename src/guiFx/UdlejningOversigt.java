/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package guiFx;

import application.controller.Controller;
import application.model.Kunde;
import application.model.Ordrelinje;
import application.model.ProduktKategori;
import application.model.Udlejning;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class UdlejningOversigt extends Stage {

	private Udlejning udlejning;
	private Kunde kunde;
	boolean pantBetaling;
	BetalingUdlejningVindue bv;

	public UdlejningOversigt(String title, Udlejning udlejning, Kunde kunde) {
		this.initStyle(StageStyle.UTILITY);
		this.initModality(Modality.APPLICATION_MODAL);
		this.setResizable(false);

		this.udlejning = udlejning;
		this.kunde = kunde;

		this.setTitle(title);
		GridPane pane = new GridPane();
		this.initContent(pane);
		pane.setPrefHeight(500);

		Scene scene = new Scene(pane);
		this.setScene(scene);
	}

	private Label lblOverskrift, lblKunder, lblStatus, lblUdlejedeProdukter, lblForbrugteProdukter, lblSamletPris,
			lblAfleveredeProdukter, lblForbrugtVare,
			lblInfo, lblFejl, lblObs;
	private TextArea txaInfo, txaSamletPris;
	private TextField txfStatus, txfProduktNavn, txfAntal;
	private Button btnBetaling, btnBetalPant, btnAfleverProdukt;
	private ListView<Kunde> lvwKunder = new ListView<>();
	private ListView<ProduktKategori> lvwKategorier = new ListView<>();
	private ListView<Ordrelinje> lvwUdlejedeProdukter = new ListView<>();
	private ListView<Ordrelinje> lvwAfleveredeProdukter = new ListView<>();

	private CheckBox cbxBrugt = new CheckBox();

	private void initContent(GridPane pane) {
		pane.setPadding(new Insets(10));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setGridLinesVisible(false);

		this.lblOverskrift = new Label("Oversigt over kunder - og udlejninger");
		pane.add(this.lblOverskrift, 0, 0);

		this.lblFejl = new Label();
		pane.add(this.lblFejl, 1, 0, 2, 1);
		this.lblFejl.setTextFill(Color.RED);

		this.lblObs = new Label();
		pane.add(this.lblObs, 6, 4);
		this.lblObs.setText("Pant skal først betales");
		this.lblObs.setTextFill(Color.RED);

		// Række 1		
		this.lblKunder = new Label("Kunder");
		pane.add(this.lblKunder, 0, 2);
		pane.add(this.lvwKunder, 0, 3);
		this.lvwKunder.getItems().setAll(Controller.getKunder());
		this.lvwKunder.getSelectionModel().select(this.kunde);

		this.lblStatus = new Label("Betalingsstatus");
		pane.add(this.lblStatus, 0, 4);
		this.txfStatus = new TextField();
		pane.add(this.txfStatus, 0, 5, 1, 3);
		this.txfStatus.setPrefHeight(300);
		this.txfStatus.setFont(new Font(15));
		this.txfStatus.setStyle("-fx-background-color: pink");
		this.txfStatus.setText("Antal produkter: " + this.lvwKunder.getSelectionModel().getSelectedItem()
				.getUdlejninger().get(0).getOrdrelinjer().size());

		// Række 2
		this.lblUdlejedeProdukter = new Label("Produkter (ikke afleverede)");
		pane.add(this.lblUdlejedeProdukter, 1, 2);
		pane.add(this.lvwUdlejedeProdukter, 1, 3, 4, 1);
		this.lvwUdlejedeProdukter.setPrefHeight(500);
		this.lvwUdlejedeProdukter.getItems().setAll(Controller.getOrdrelinjer(this.kunde));
		this.lvwUdlejedeProdukter.getSelectionModel().selectedIndexProperty()
				.addListener((p, ov, nv) -> this.selectedProduktChanged());
		this.lvwKunder.getSelectionModel().selectedIndexProperty()
				.addListener((p, ov, nv) -> this.selectedKundeChanged());

		this.lblForbrugteProdukter = new Label("Antal forbrugte produkter");
		pane.add(this.lblForbrugteProdukter, 1, 4);

		this.txfProduktNavn = new TextField();
		this.txfProduktNavn.setPromptText("Produkt Navn");
		this.txfProduktNavn.setEditable(false);
		pane.add(this.txfProduktNavn, 1, 5, 2, 1);
		this.txfAntal = new TextField();
		this.txfAntal.setPromptText("Antal");
		pane.add(this.txfAntal, 3, 5);
		this.txfAntal.setPrefWidth(50);
		this.txfAntal.setEditable(false);

		this.lblForbrugtVare = new Label("Er varen brugt?");
		pane.add(this.lblForbrugtVare, 4, 4);
		pane.add(this.cbxBrugt, 4, 5);
		this.cbxBrugt.setDisable(true);

		this.btnAfleverProdukt = new Button("Aflever produkt");
		pane.add(this.btnAfleverProdukt, 1, 6, 4, 1);
		this.btnAfleverProdukt.setPrefSize(300, 100);
		this.btnAfleverProdukt.setOnAction(event -> afleverProduktAction());
		this.btnAfleverProdukt.setDisable(true);

		// Række 3
		this.lblAfleveredeProdukter = new Label("Afleverede produkter");
		pane.add(this.lblAfleveredeProdukter, 5, 2);
		pane.add(this.lvwAfleveredeProdukter, 5, 3);
		this.lvwAfleveredeProdukter.getItems()
				.setAll(this.lvwKunder.getSelectionModel().getSelectedItem().getUdlejninger().get(0)
						.getAfleveredeUdlejninger());

		this.lblSamletPris = new Label("Samlet pris");
		pane.add(this.lblSamletPris, 5, 4);
		this.txaSamletPris = new TextArea();
		this.txaSamletPris.setPrefWidth(200);
		pane.add(this.txaSamletPris, 5, 5, 1, 2);
		this.txaSamletPris.setText("Pant til betaling nu: " + this.udlejning.getPant());

		// Række 4
		this.lblInfo = new Label("Info");
		pane.add(this.lblInfo, 6, 2);

		this.txaInfo = new TextArea();
		pane.add(this.txaInfo, 6, 3, 2, 1);
		this.txaInfo.setFont(new Font(15));
		this.txaInfo.setMaxWidth(300);
		this.txaInfo.setEditable(false);
		if (this.kunde != null) {
			this.txaInfo.setText(kundeInfo(this.kunde));
		}

		this.btnBetalPant = new Button("Betal pant");
		pane.add(this.btnBetalPant, 6, 5, 1, 2);
		this.btnBetalPant.setPrefSize(140, 300);
		this.btnBetalPant.setOnAction(event -> betalPantAction());

		this.btnBetaling = new Button("Betaling");
		pane.add(this.btnBetaling, 7, 5, 1, 2);
		this.btnBetaling.setPrefSize(140, 300);
		this.btnBetaling.setDisable(true);
		this.btnBetaling.setOnAction(event -> betalingAction());
	}

	public String kundeInfo(Kunde kunde) {

		String id = "";
		String navn = "";
		String adresse = "";
		String telefonNr = "";
		String email = "";
		String startDatoUdlejning = "";
		String slutDatoUdlejning = "";
		if (kunde != null) {
			id = "KundeId: " + kunde.getKundeID();
			navn = "Navn: " + kunde.getNavn();
			adresse = "Adresse: " + kunde.getAdresse();
			telefonNr = "Telefonnr: " + kunde.getTelefonNr();
			email = "Email: " + kunde.getEmail();
			startDatoUdlejning = "Start udlejning: " + this.udlejning.getStartDato().toString();
			slutDatoUdlejning = "Slut udlejning: " + this.udlejning.getSlutDato().toString();
		}
		return String.format("%s %n%s %n%s %n%s %n%s %n%s %n%s", id, navn, adresse, telefonNr, email,
				startDatoUdlejning,
				slutDatoUdlejning);

	}

	private void selectedKundeChanged() {
		Kunde kunde = this.lvwKunder.getSelectionModel().getSelectedItem();
		if (!this.udlejning.isPantBetalt()) {
			this.btnAfleverProdukt.setDisable(true);
			this.cbxBrugt.setDisable(true);
			this.lblObs.setText("Pant skal først betales");
		} else if (this.udlejning.isPantBetalt()) {
			this.btnAfleverProdukt.setDisable(false);
			this.cbxBrugt.setDisable(false);
			this.lblObs.setText("");
			this.lblObs.setTextFill(Color.RED);
		}
		this.lvwUdlejedeProdukter.getItems().setAll(Controller.getOrdrelinjer(kunde));
		this.txaInfo.setText(kundeInfo(kunde));

		if (kunde.getUdlejninger().get(0).isBetalt()) {
			this.txfStatus.setStyle("-fx-background-color: lightgreen");
			this.txfStatus.setText("Betalt");
		} else {
			this.txfStatus.setStyle("-fx-background-color: pink");
		}
		if (Controller.getOrdrelinjer(kunde).size() == 0) {
			this.btnBetaling.setDisable(false);
		} else {
			this.btnBetaling.setDisable(true);
		}

		this.lvwKategorier.getItems().setAll(Controller.getUdlejningKategori());
		this.lvwAfleveredeProdukter.getItems().setAll(kunde.getUdlejninger().get(0).getAfleveredeUdlejninger());
		this.lvwUdlejedeProdukter.getSelectionModel().select(0);
		this.txaSamletPris.setText(
				"Samlet pant til betaling nu: " + this.udlejning.getPant());
	}

	private void selectedProduktChanged() {
		Ordrelinje produkt = this.lvwUdlejedeProdukter.getSelectionModel().getSelectedItem();
		if (produkt != null) {
			this.txfProduktNavn.setText(produkt.getProdukt().getNavn());
			this.txfAntal.setText("" + produkt.getAntal());
		}
		if (this.udlejning.isPantBetalt()) {
			this.btnAfleverProdukt.setDisable(false);
			this.cbxBrugt.setDisable(false);
		}
	}

	// Aflevering af udlejede produkter - metode
	private void afleverProduktAction() {
		Kunde kunde = this.lvwKunder.getSelectionModel().getSelectedItem();
		Udlejning udlejning = kunde.getUdlejninger().get(0);
		Ordrelinje ordrelinje = this.lvwUdlejedeProdukter.getSelectionModel().getSelectedItem();
		boolean produktetErBrugt = this.cbxBrugt.isSelected();

		double samletPris = 0;
		int antal = 0;
		int samletAntalProdukter = 0;

		String strProduktPriser = "Produkt priser: ";
		String strPant = "Pant: ";
		String strLightGreen = "-fx-background-color: lightgreen";
		String strPink = "-fx-background-color: pink";
		String strBetalt = "Afleveret: Gå til betaling";
		String strAntalProdukter = "Antal produkter: ";

		ordrelinje.setBrugtUdlejetProdukt(produktetErBrugt);

		Controller.removeOrdreLinjeFromOrdre(udlejning, ordrelinje);
		Controller.returnereUdlejedeProdukter(udlejning, ordrelinje);
		if (ordrelinje != null) {
			this.lvwAfleveredeProdukter.getItems()
					.setAll(this.kunde.getUdlejninger().get(0).getAfleveredeUdlejninger());
		}

		// Retunerer samlet antal og pris for afleverede produkter
		for (Ordrelinje p : this.lvwAfleveredeProdukter.getItems()) {
			samletPris += p.udregnOrdrelinjeSamletPris();
			antal += p.getAntal();
		}

		// Returnere samlet antal udlejede produkter som kunden har lejet
		for (Ordrelinje o : Controller.getOrdrelinjer(kunde)) {
			samletAntalProdukter += o.getAntal();
		}

		this.txaSamletPris
				.setText(strAntalProdukter + ": " + antal + "\n" + strProduktPriser + ": " + samletPris + "\n" + strPant
						+ this.udlejning.getPant());

		if (Controller.getOrdrelinjer(kunde).size() == 0) {
			this.txfStatus.setStyle(strLightGreen);
			this.txfStatus
					.setText(strBetalt);
			this.txfProduktNavn.setText("");
		} else if (this.lvwUdlejedeProdukter.getItems().size() > 0) {
			this.txfStatus.setStyle(strPink);
			this.txfStatus
					.setText(strAntalProdukter + samletAntalProdukter);
		}
		this.lvwUdlejedeProdukter.getItems().setAll(Controller.getOrdrelinjer(kunde));
		this.lvwUdlejedeProdukter.getSelectionModel().select(0);
		if (Controller.getOrdrelinjer(kunde).size() == 0) {
			this.pantBetaling = false;
			this.btnBetalPant.setDisable(true);
			this.btnBetaling.setDisable(false);
			this.udlejning.setPantBetalt(false);
			this.btnAfleverProdukt.setDisable(true);
		}
	}

	// Betal pant metode
	private void betalPantAction() {
		this.pantBetaling = true;
		Kunde kunde = this.lvwKunder.getSelectionModel().getSelectedItem();

		// Åbner vinduet til betaling af PANT
		this.bv = new BetalingUdlejningVindue(
				"Betal pant - Panten skal betales ved de udlejede vares afhentning", this.udlejning, kunde,
				this.pantBetaling);
		this.bv.showAndWait();
	}

	// Betal udlejede produkter ordre
	private void betalingAction() {
		this.pantBetaling = false;
		Kunde kunde = this.lvwKunder.getSelectionModel().getSelectedItem();
		if (this.lvwAfleveredeProdukter.getItems().size() == 0) {
			this.lblFejl.setText("Der er intet at betale. Der er ingen afleverede produkter.");
			return;
		}

		// Åbner vinduet til betaling af hele udlejningsordren
		this.bv = new BetalingUdlejningVindue(
				"Betal ordre - Panten skal modregnes fra den samlede ordre", this.udlejning, kunde,
				this.pantBetaling);
		this.bv.showAndWait();
	}
}
