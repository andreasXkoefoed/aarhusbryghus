/**
 * Aarhus bryghus Tværfagligt projekt 2.semester
 * Andreas Koefoed-Pedersen
 * Nassim Ghonem
 * Emilio Iezzi Højland Agger
 */
package guiFx;

import application.controller.Controller;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class MainApp extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void init() throws Exception {
		Controller.loadStorage();
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("Aarhus Bryghus");
		GridPane pane = new GridPane();
		this.initContent(pane);
		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.initStyle(StageStyle.UTILITY);
		stage.setResizable(false);
		stage.setHeight(350);
		stage.setWidth(500);
		stage.show();
	}

	@Override
	public void stop() {
		Controller.saveStorage();
	}

	// -------------------------------------------------------------------------
	private LagerVindue lager;
	private ButikVindue butik;

	private void initContent(GridPane pane) {
		pane.setGridLinesVisible(false);
		pane.setPadding(new Insets(20));
		pane.setHgap(10);
		pane.setVgap(10);
		pane.setAlignment(Pos.CENTER);

		Button buttonLagerPane = new Button("Lageret");
		buttonLagerPane.setPrefSize(150, 150);
		pane.add(buttonLagerPane, 0, 1);
		buttonLagerPane.setOnMouseClicked(e -> lagerAction());

		Button buttonButikPane = new Button("Butikken");
		buttonButikPane.setPrefSize(150, 150);
		pane.add(buttonButikPane, 2, 1);
		buttonButikPane.setOnMouseClicked(e -> butikAction());
	}

	private void lagerAction() {
		this.lager = new LagerVindue("Lageret");
		this.lager.showAndWait();
	}

	private void butikAction() {
		this.butik = new ButikVindue("Butikken");
		this.butik.showAndWait();
	}

}
